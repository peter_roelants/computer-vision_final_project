% Define document
\documentclass[11pt]{scrartcl}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{verbatim} % comments
\usepackage{parskip} % split paragraphs by vertical space
\usepackage{graphicx}
\usepackage{url}
\usepackage{hyperref}
\usepackage{placeins}
\usepackage{amsmath}
\usepackage{todonotes}
\usepackage{subcaption}
\usepackage{booktabs}
\usepackage{multirow}



\begin{document}



\input{titlepage.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
This report describes the detection of incisors in panoramic radiographs with the help of active shape models. The first section of this report describes the use of active shape models, and analyses the obtained teeth models. The second section describes the image preprocessing techniques used to preprocess the image before the models are fitted. The third section describes the fitting of the active shape models to the preprocessed radiographs. Results obtained with the fitting of the active shape model are shown in section four.





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Active Shape Model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Active Shape Model}
Active shape models are used to detect the incisors in panoramic radiographs. The active shape model implemented is based on the study by Cootes et al. \cite{Cootes_asm_train_application, Cootes_IntroToASM, Cootes00statisticalmodels}. Active shape models are statistical models that describe shape variations. The model is build from training examples provided as landmark points that describe a shape. To build the model first the shapes are aligned by Procrustes analysis. Once the models are aligned a statistical model of the shape is build by the use of principal component analysis (PCA).

In this paper three different models are build from the incisors: a model that contains all 8 teeth, separate modeling of lower and upper incisors, and model for each incisor separately.


% Shape alignment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Shape alignment}
To align the training set of shapes Procrustes analysis \cite{Procrustes} based on least squares approximation is used. This method tries to find a transformation $T_i$ for each shape $\mathbf{x}_i$ that performs a translation $X_i, Y_i$, scale $s$, and rotation $\theta$ in order to minimize the sum of squares between the points of all shapes $\mathbf{x}_i$ and the average shape $\bar{\mathbf{x}}$.

First all the vectors $\mathbf{x}_i$ are translated so their centroids are at the origin. This translation defines $X_i$ and $Y_i$. Next the optimal scale $s$ and rotation matrix $R_{\theta}$ are found iteratively by calculating the mean $\bar{\mathbf{x}}$ of the current transformed shapes $T_i(\mathbf{x}_i)$, normalizing this mean so $|\bar{\mathbf{x}}| = 1$, and find the optimal $s$ and $R_{\theta}$ by solving the equation:

\begin{equation} \label{eq:procrustes_least_squares}
\text{min} \, | s R_{\theta} \mathbf{x}_i - \bar{\mathbf{x}} |^2
\end{equation}

This is done until the mean shape $\bar{\mathbf{x}}$ doesn't change anymore between iterations.


% Principal component analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Principal component analysis}
\label{sec:pca}
After the shapes are aligned principal component analysis (PCA) is used to project the data of each shape $\mathbf{x}_i$ into principal components (PC) to get a model $\mathbf{b}_i$. This is done so that the first PC captures the greatest variance in the dataset, and the next orthogonal PCs capture each the next greatest variance in the dataset. Since each PC captures less variance than the previous PC we can reduce the dimensionality of the data by only selecting the top $p$ PCs that capture at least a certain percentage of the variance of the total dataset. In the report the top $p$ principal components that describe at least 99\% of the variance in the dataset are selected. The next paragraphs describe three different models that are build. Each model is build from all data, including the mirrored landmarks.



% Model of all 8 incisors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Model of all 8 incisors}
The first model models all 8 incisors together. This can have as benefit that the alignment of each tooth to each other tooth is included in the model. Neighboring teeth can influence the position of each other, although this is not necessarily always the case. A disadvantage is that the contours of the teeth will be more difficult to model: the model has to find a compromise to model the shape of all teeth. The cumulative variance of the principal components is shown in figure \ref{fig:cumvar_all_8_incisors}. It can be noted in this figure that 12 principal components can explain 99\% of the total variance in the dataset.

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.8\textwidth]{img/cumvar_all_teeth.png}
  \caption{Cumulative variance for each principal component for the model of all 8 incisors. The red line represents the 99\% mark.}
  \label{fig:cumvar_all_8_incisors}
\end{figure}

To illustrate the influence of the principal components two figures are shown in figure \ref{fig:variance_influcence_all_teeth}. Figure \ref{fig:variance_influcence_all_teeth_pc1} and \ref{fig:variance_influcence_all_teeth_pc2} show the effect of varying the model parameters corresponding to the first, and respectively the second principal component. The variation decreases or increases the corresponding model parameter with one standard deviation with respect to the zero model that has all model parameter in $\mathbf{b}$ equal to zero. It can be noted in the resulting figures that the generated images don't necessarily need to correspond to probable teeth. This can be a result of non-linear dependencies between the landmarks in the data.



\begin{figure}
\centering
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{img/all_teeth_influence_pc1.png}
  \caption{Influence of the variance on the first principal component.}
  \label{fig:variance_influcence_all_teeth_pc1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{img/all_teeth_influence_pc2.png}
  \caption{Influence of the variance on the second principal component.}
  \label{fig:variance_influcence_all_teeth_pc2}
\end{subfigure}
\caption{Influence of the variance on the principal components. Left (green) is -1 standard deviation, middle (black) is the zero model, right (red) is +1 standard deviation.}
\label{fig:variance_influcence_all_teeth}
\end{figure}




% Model of separate lower and upper incisors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Model of separate lower and upper incisors}
The second model models the lower and upper incisors separately. This can have as benefit that the alignment of each tooth to each other tooth is included in the model without the dependency of the lower teeth on the upper teeth. A disadvantage is that the dependency between the lower and upper incisors is lost. The cumulative variance of the principal components of the upper and lower incisors are shown in figure \ref{fig:cumvar_all_upper_incisors} and \ref{fig:cumvar_all_lower_incisors} . It can be noted in this figure that 10 and 8 principal components can explain 99\% of the total variance in the dataset for the lower and respectively the upper incisors.

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.85\textwidth]{img/cumvar_upper_teeth.png}
  \caption{Cumulative variance for each principal component for the model of the 4 upper incisors. The red line represents the 99\% mark.}
  \label{fig:cumvar_all_upper_incisors}
\end{figure}

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.85\textwidth]{img/cumvar_lower_teeth.png}
  \caption{Cumulative variance for each principal component for the model of the 4 lower incisors. The red line represents the 99\% mark.}
  \label{fig:cumvar_all_lower_incisors}
\end{figure}



The influence of the first principal components of the lower and upper incisor models are shown in figure \ref{fig:variance_influcence_upper_lower_teeth}.  Figure \ref{fig:variance_influcence_upper_teeth_pc1} and \ref{fig:variance_influcence_lower_teeth_pc2} show the effect of varying the model parameters corresponding to the first, and respectively the second principal components. As with the previous model it can be noted in the resulting figures that the generated images don't necessarily need to correspond to probable teeth.

\begin{figure}
\centering
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{img/upper_influence_pc1.png}
  \caption{Influence of the variance on the first principal component of the upper incisors.}
  \label{fig:variance_influcence_upper_teeth_pc1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{img/lower_influence_pc1.png}
  \caption{Influence of the variance on the first principal component of the lower incisors.}
  \label{fig:variance_influcence_lower_teeth_pc2}
\end{subfigure}
\caption{Influence of the variance on the principal components. Left (green) is -1 standard deviation, middle (black) is the zero model, right (red) is +1 standard deviation.}
\label{fig:variance_influcence_upper_lower_teeth}
\end{figure}


% Model of separate incisors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Model of separate incisors}
The third model models each incisor separately. This can have as benefit that there is a more specific model for each incisor and that the shape contour can be matched more specifically. A disadvantage is that the dependency between all separate incisors is lost.
The cumulative variance of the principal components of the first incisor is shown in figure \ref{fig:cumvar_separate_incisors}. It can be noted in this figure that 4 principal components can explain 99\% of the total variance in the dataset. This is the same for each separate incisor.

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.85\textwidth]{img/cumvar_segment_1.png}
  \caption{Cumulative variance for each principal component for the model of the first separate incisor. The red line represents the 99\% mark.}
  \label{fig:cumvar_separate_incisors}
\end{figure}

The influence of the first, second, and third principal components of the second incisor model are shown in figure \ref{fig:variance_influcence_segments}. As with the previous model it can be noted in the resulting figures that the generated images don't necessarily need to correspond to probable teeth.


\begin{figure}
\centering
\begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{img/segment_1_influence_pc1.png}
  \caption{First principal component of the second incisor.}
  \label{fig:variance_influcence_seg_2_pc1}
\end{subfigure}%
\begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{img/segment_1_influence_pc2.png}
  \caption{Second principal component of the second incisor.}
  \label{fig:variance_influcence_seg_2_pc2}
\end{subfigure}%
\begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{img/segment_1_influence_pc3.png}
  \caption{Third principal component of the second incisor.}
  \label{fig:variance_influcence_seg_2_pc3}
\end{subfigure}%

\caption{Influence of the variance on the principal components. Left (green) is -1 standard deviation, middle (black) is the zero model, right (red) is +1 standard deviation.}
\label{fig:variance_influcence_segments}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preprocessing of the dental radiographs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\FloatBarrier
\section{Preprocessing of the dental radiographs}



The radiographs used for this project are noisy and have local differences in brightness and contrast. It can also be hard to distinguish teeth from bone.
Several image enhancement techniques are used to overcome these problems and make the implementation more robust. Some steps of this process are shown in figure \ref{fig:prepro}. 
Preprocessing is done with edge detection kept in mind. 

The images have vertical bands of increased brightness which seem to negatively influence the other contrast enhancement techniques.
These bands can be removed by building a profile and extracting them from the image. The profile is built by blurring the image to minimize noise interference, and then picking the darkest value for each column.
Most of the bands are removed with minimal error, and contrast isn't reduced.
Afterwards, noise is removed with a Gaussian blur. 

To improve contrast a contrast limited adaptive histogram equalization (CLAHE) is used as proposed by numerous papers like \cite{Ahmad_Xray_CLAHE}.
This technique is widely used in (dental) radiographs both as a preprocessing step for computer vision and to make reading the images easier for professionals.
Adaptive histogram equalization is a technique used to enhance contrast locally so details can be seen more clearly. By limiting the contrast for homogeneous regions, over-amplification of remaining noise is prevented.
On the images, this enhancement has the effect of bringing out the shape of the teeth in regions with lower contrast. 

After these steps the teeth and bones can be seen clearly as lighter regions with dark spaces between them. Morphological transformations can be used to emphasize the smaller lighter regions which represent mostly teeth, and to make the tight dark spaces clearer. 
Top hat and bottom hat transformations can be use to extract light (or dark) objects on a slowly changing dark (or light) background \cite{Zhou_Xray_Hats}. The result of the top hat transform is added to the image, and the result of bottom hat filter is subtracted.
The size of the hat transforms can be adjusted so that the biggest contrast difference is mostly at the edges of the teeth, which will be what we're going to look for. 

To highlight the edges a Sobel filter is applied along both axes. Additionally, the derivation provides information about the orientation of the edges, and global brightness differences between images are ignored.
Morphological transformations are used again to highlight the black and white lines that represent the edges.

\begin{figure}
\centering
\begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{img/prepro/13.png}
  \caption{Original radiograph.}
  \label{fig:filter_start}
\end{subfigure}%
\begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{img/prepro/13c.png}
  \caption{Radiograph after contrast enhancement.}
  \label{fig:filter_before_sobel}
\end{subfigure}%
\begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{img/prepro/13e.png}
  \caption{Radiograph after Sobel filtering.}
  \label{fig:filter_after_sobel}
\end{subfigure}%
\caption{Preprocessing of radiograph 13 during certain steps of the process.}
\label{fig:prepro}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting the model to the image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fitting the model to the image}

The iterative algorithm of fitting the model to the image begins with finding a good starting position to place the mean shape. 
This is done by using an appearance model and domain knowledge. Horizontally, the teeth are centered in each image. This is done by the radiographer.
This means that only the reference along the vertical axis needs to be found. The middle of the gap between the teeth is detected by moving a ellipse over the image that computes the mean in this elliptical mask. The position that results in the lowest mean is chosen as the center point on the y-axis.
Using the known average distances relative to this reference point, the mean shape can be placed on the image and start the first iteration. 

With each iteration the shape is updated by finding new plausible landmark points and constraining the shape to fit the model. This is done for 300 iterations, after which the algorithm converges and the contours of the teeth are found.
The new points are chosen by looking along the normals to the shape boundary on either side of each point. 

To decide on a good new landmark point, a statistical model of what an edge should look like is computed for each point separately \cite{Cootes_IntroToASM}. This model is build with the images from the training set and is shown in figure \ref{fig:prepro_mean_sample}.
That way the fact that shapes don't always follow the strongest edge, but possibly just a distinct one can be captured.
Because a Sobel filter is used for preprocessing, the model can also partially capture the direction of the edge at each point. 
The model consists of the mean and covariance matrix for each sample line of about 25 pixels along the normal on each side of the point. The model is compared to normalized samples from a line of about 44 pixels on each side of the point. The best match is found by sliding the mean line over the new sampled line, and using the Mahalanobis distance measure to find the best match.

The new set of landmark points usually don't form a shape which fits the model. In stead, the parameters of the shape $(X_t,Y_t,s,\theta,\mathbf{b})$ are updated and constrained so their Mahalanobis distance from the zero model is plausible up to 95\% certainty. After this, a new iteration starts. 

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.8\textwidth]{img/prepro/mean_sample_filter.png}
  \caption{The mean of all sample lines for each point used to match the sample line of the shape that is being fit. Each point on the x-axis contains the mean line of the corresponding landmark. The y-axis show the pixels at the corresponding position for each line, index 25 is the middle of the mean sample line.}
  \label{fig:prepro_mean_sample}
\end{figure}







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Evaluation}
The evaluation of the models fitted for the three different models from section \ref{sec:pca} is done on a test set that consists of the first three radiographs from the given dataset. The training set used to build the models consist of the remaining radiographs and corresponding shapes. Mirrored shapes are also included in the training set.

To evaluate the model fitting two different distance measures are computed. The first distance measure computes the mean Euclidean distance between each two corresponding points in the fitted model and the target model and is shown in equation \ref{eq:mean_ED}. The second distance measure computes the maximum Euclidean distance between each two corresponding points in the fitted model and the target model and is shown in equation \ref{eq:max_ED}. 

\begin{equation} \label{eq:mean_ED}
\text{Mean Euclidean Distance} = \frac{1}{n} \sum_{i=0}^{n} d(p_i - q_i)
\end{equation}

\begin{equation} \label{eq:max_ED}
\text{Maximum Euclidean Distance} = \text{max}( \, d(p_i - q_i)) \quad \forall i = 0 \ldots n
\end{equation}

The results obtained on the three test samples with these distance measures are shown in table \ref{tab:distance_results}. Figure \ref{fig:fit_img_2} shows the fit for each model on radiograph 2. It can be noted that in these results the first model, that combines all 8 incisors into one model, gets the best results. While the correct positions of the incisors are mostly found in this model, the shape contour for each incisor is only a very rough fit.

It can also be noted that the model that contains each incisor separately gets the worst results. This is because the dependencies between the teeth get lost in this model. As can be noted in figure \ref{fig:fit_segment_img2} most incisors are found, but once in a while a single incisor diverges from the real position, leading to large distances. This divergence does not happen in the model with all 8 incisors because the positions of the teeth relative to each other is contained in this model. The shape contours of the lower incisors that have converged are fitted very well, and are less rough than in the model with all 8 incisors together.


After analyzing these results a better model could be developed. Because the model that combines all 8 incisors finds the positions of all incisors most of the time it could be used to first fit a rough approximation of each incisor position. Afterwards each incisor could be fitted separately with as initial position the position found by the first model. Because the first model finds good approximations of each position, less divergence should occur while fitting the incisors separately, and limits can be introduced to limit the possible positions of the separate incisors to stop the model from diverging. This hierarchical model was not tested because of the time constraints related to this assignment.




\begin{table}[!htbp]
  \centering
  \begin{tabular}{l l r r r r}
    \toprule
    \textbf{Image} & \textbf{Distance} & \textbf{All} & \textbf{Lower/Upper} & \textbf{separate} \\
    \midrule
    \multirow{2}{*}{1} & 
        \multicolumn{1}{l}{Mean} & \multicolumn{1}{r}{24.00} & \multicolumn{1}{r}{25.45} & \multicolumn{1}{r}{36.30} \\
        & \multicolumn{1}{l}{Max} & \multicolumn{1}{r}{114.01} & \multicolumn{1}{r}{112.64} & \multicolumn{1}{r}{110.87} \\
    \hline
    \multirow{2}{*}{2} & 
        \multicolumn{1}{l}{Mean} & \multicolumn{1}{r}{31.79} & \multicolumn{1}{r}{46.84} & \multicolumn{1}{r}{46.92} \\
        & \multicolumn{1}{l}{Max} & \multicolumn{1}{r}{86.84} & \multicolumn{1}{r}{84.49} & \multicolumn{1}{r}{193.56} \\
    \hline
    \multirow{2}{*}{3} & 
        \multicolumn{1}{l}{Mean} & \multicolumn{1}{r}{30.19} & \multicolumn{1}{r}{76.04} & \multicolumn{1}{r}{65.77} \\
        & \multicolumn{1}{l}{Max} & \multicolumn{1}{r}{77.50} & \multicolumn{1}{r}{150.94} & \multicolumn{1}{r}{295.144} \\
    \midrule
    \multirow{2}{*}{Avg} & 
        \multicolumn{1}{l}{Mean} & \multicolumn{1}{r}{28.66} & \multicolumn{1}{r}{49.44} & \multicolumn{1}{r}{49.66} \\
        & \multicolumn{1}{l}{Max} & \multicolumn{1}{r}{92.78} & \multicolumn{1}{r}{116.02} & \multicolumn{1}{r}{199.86} \\
     
    \bottomrule
  \end{tabular}
  \caption{Mean Euclidean Distance (Mean) and Maximum Euclidean Distance (Max) for the three test images for the three different models. The last row is the average of the distances for each model.}
  \label{tab:distance_results}
\end{table}



\begin{figure}
\centering
\begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{img/fit_all_img2.png}
  \caption{Model of all 8 incisors.}
  \label{fig:fit_all_img2}
\end{subfigure}%
\begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.88\linewidth]{img/fit_lower_upper_img2.png}
  \caption{Model of separate lower and upper incisors.}
  \label{fig:fit_lower_upper_img2}
\end{subfigure}%
\begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.81\linewidth]{img/fit_segment_img2.png}
  \caption{Model of separate incisors.}
  \label{fig:fit_segment_img2}
\end{subfigure}%
\caption{Fitting results of the incisors on radiograph 2 for the three different models.}
\label{fig:fit_img_2}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conclusion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\FloatBarrier
\section{Conclusion}

While the active shape model converges most of the time to find the positions of the incisors, the shape contours are not perfectly fitted. A model that uses all 8 incisors into one single active shape model gives the best results with finding all the incisors, but is not able to find accurate shape contours. 




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{abbrv}
\bibliography{bibliograph}


\end{document}
