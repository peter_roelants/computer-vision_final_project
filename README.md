Computer Vision Final Project
=============================

Data
----

Copy the `_Data` folder from the zip provided by the assignment on Toledo to the base folder of this project. The data is not included in the git repository because it contains of lots of images.