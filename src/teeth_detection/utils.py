#####################################################################
# Utility functions
#####################################################################

import numpy as np


def normalize_abs(line):
    """
    Normalise the given line with the sum of the absolute values
    Input:
        line: 1D numpy array
    Output:
        line
    """
    sum_abs = np.sum(np.absolute(line))
    if sum_abs == 0:
        return line
    return line / sum_abs


def normalize_abs_grey(line):
    """
    Normalise the given line with the sum of the absolute values
    Input:
        line: 1D numpy array
    Output:
        line
    """
    grey = 128
    line = line - grey
    sum_abs = np.sum(np.absolute(line))
    
    if sum_abs == 0:
        return line + grey
    return (line / sum_abs) + grey

def normalize_min_max(line):
    """
    Normalise the given line between 0 and 1
    Input:
        line: 1D numpy array
    Output:
        line
    """
    amin = np.amin(line)
    amax = np.amax(line)
    arange = (amax - amin)
    if arange == 0:
        return (line - amin)
    return (line - amin) / arange




def squared_mahalanobis_distance(x, mean, S):
    """
    Computes the squared mahalanobis distance of x to the mean (0, ..., 0) with covariance S
    Input:
        x: vector in model space to compute the distance of
        mean: mean of models
        S: covariance matrix normalised, aligned shapes
    Output:
        squared mahalanobis distance
    """
    x_dev = x - mean
    S_inv = np.linalg.pinv(S)
    return x_dev.T.dot(S_inv).dot(x_dev)


def normalize(v):
    """
    normalize vector v
    """
    return v / np.linalg.norm(v)


def mean_euclidian_dist(s1, s2):
    """
    Return the mean euclidian distance between the 2 shapes.
    """
    nb_of_points = s1.nb_of_points()
    sum_dist = 0
    for i in xrange(nb_of_points):
        sum_dist += np.sqrt( (s1.x(i) - s2.x(i))**2 + (s1.y(i) - s2.y(i))**2 )
    return sum_dist / nb_of_points


def max_euclidian_dist(s1, s2):
    """
    Return the maximum euclidian distance between the 2 shapes.
    """
    nb_of_points = s1.nb_of_points()
    max_dist = 0
    for i in xrange(nb_of_points):
        dist = np.sqrt( (s1.x(i) - s2.x(i))**2 + (s1.y(i) - s2.y(i))**2 )
        if dist > max_dist:
            max_dist = dist
    return max_dist


def chi_square_value(df):
    """
    Return the chi-square values of the degree of freedom with alfa = 0.025
    """
    if df == 1:
        return 5.024
    elif df == 2:
        return 7.378
    elif df == 3:
        return 9.348
    elif df == 4:
        return 11.143
    elif df == 5:
        return 12.833
    elif df == 6:
        return 14.449
    elif df == 7:
        return 16.013
    elif df == 8:
        return 17.535
    elif df == 9:
        return 19.023
    elif df == 10:
        return 20.483
    elif df == 11:
        return 21.920
    elif df == 12:
        return 23.337
    elif df == 13:
        return 24.736
    elif df == 14:
        return 26.119
    elif df == 15:
        return 27.488
    elif df == 16:
        return 28.845
    elif df == 17:
        return 30.191
    elif df == 18:
        return 31.526
    elif df == 19:
        return 32.852
    elif df == 20:
        return 34.170
    else:
        return 46.979
