#####################################################################
# PCA analysis of shapes
#####################################################################

import numpy as np

import shape as shape_utils
from shape import Shape

def get_empty_model(nb_of_pc):
    """
    Get the initial pca model
    """
    return np.zeros(nb_of_pc, dtype=float)


def pca_of_shapes(shapes, nbOfPc=None):
    """
    Do a PCA analysis on the list of shapes
    Input:
        shapes: list of shapes
    Output:
        eigenvalues, eigenvectors, mean_shape
    """
    nb_of_shapes = len(shapes)
    shape_size = shapes[0].size()
    shapes_matrix = np.ndarray(shape=(nb_of_shapes,shape_size))
    for idx,shape in enumerate(shapes):
        shapes_matrix[idx,:] = shape.xy_1D()
    eigenvalues, eigenvectors, mu = pca(shapes_matrix)
    mean_shape = Shape(mu, shapes[0].split_points)
    if nbOfPc == None:
        return eigenvalues, eigenvectors, mean_shape
    else:
        return eigenvalues[0:nbOfPc], eigenvectors[:,0:nbOfPc], mean_shape


def pca(X):
    '''
    Do a PCA analysis on X
    Input:
        X: array of flat shapes (numpy.ndarray)
            X.shape: (nb of shapes, nb of shape points*2)
            1-st D: holds each shape
            2-nd D: holds the points in the shape
            X[s,2*n] holds the x-pos for point n of shape s
            X[s,2*n+1] holds the y-pos for point n of shape s
    Output:
        eigenvalues: eigenvalues of X^T X
            eigenvalues.shape: (shapes,)
        eigenvectors: eigenvectors belonging to the eigenvalues
            eigenvectors.shape: (points, shapes)
        mu: mean shape as flat shape
            mu.shape: (points,)
    '''
    # compute the average shape
    mu = np.mean(X, axis=0)
    # compute the deviation from the mean matrix
    devX = (X - mu)
    # compute the covariance matrix X X^T
    covX = devX.dot(devX.T)
    # compute the eigenvalues and eigenvectors of the covariance matrix
    eigenvalues, v = np.linalg.eigh(covX)
    # get the eigenvectors of X^T X
    eigenvectors = devX.T.dot(v)
    # normalize the new eigenvectors
    eigenvectors = eigenvectors / np.linalg.norm(eigenvectors, axis=0)
    # Get the top nb_components components by sorting the eigenvalues
    idxs = np.argsort(-eigenvalues)
    eigenvalues = eigenvalues[idxs]
    eigenvectors = eigenvectors[:, idxs]
    return eigenvalues, eigenvectors, mu


def project(X, eigvec, mu):
    '''
    Project X on the space spanned by the eigenvectors.
    Input:
        X: flat array
        eigvec: eigenvectors to project with
        mu: the average flat array
    Output:
        projection: projected model onto the eigenvectors
    '''
    devX = (X - mu)
    projection = devX.dot(eigvec)
    return projection


def project_shape(shape, eigvec, mean_shape):
    '''
    Project shape on the space spanned by the eigenvectors.
    Input:
        shape: shape to project
        eigvec: eigenvectors to project with
        mean_shape: the mean shape
    Output:
        projection: projected model onto the eigenvectors
    '''
    return project(shape.xy_1D(), eigvec, mean_shape.xy_1D())


def reconstruct(b, eigvec, mu):
    '''
    Reconstruct an image based on its PCA-coefficients b, the eigenvectors and the average mu.
    Input:
        b: PCA-coefficients
        eigvec: eigenvectors to project with
        mu: the average flat array
    Output:
        projection: flat shape
    '''
    return mu + b.dot(eigvec.T)

def reconstruct_shape(b, eigvec, mean_shape):
    '''
    Reconstruct an image based on its PCA-coefficients b, the eigenvectors and the average mu.
    Input:
        b: PCA-coefficients
        eigvec: eigenvectors to project with
        mean_shape: the mean shape
    Output:
        projection: projected shape
    '''
    new_shape_array = reconstruct(b, eigvec, mean_shape.xy_1D())
    return Shape(new_shape_array, mean_shape.split_points)


def get_cov_mean_all_models(shapes, eigenvectors):
    """
    Return the covariance matrix and mean model of all the shapes
    """
    mean_shape = shape_utils.mean_shape(shapes)
    nb_of_shapes = len(shapes)
    b_size = eigenvectors.shape[1]
    bs = np.ndarray(shape=(nb_of_shapes, b_size))
    for idx, shape in enumerate(shapes):
        bs[idx,:] = project_shape(shape, eigenvectors, mean_shape)
    return np.cov(bs.T)

