#####################################################################
# Utility functions tests
#####################################################################

import numpy as np

import utils


def test_squared_mahalanobis_distance_colinear():
    """
    """
    # create data (5 samples, 3D)
    independent_samples = np.random.randn(5, 3)
    independent_samples[:,2] = 1
    independent_samples[1,2] = 1.0000000001
    mean = np.mean(independent_samples, axis=0)
    cov = np.cov(independent_samples.T)
    print('independent_samples.shape: ' + str(independent_samples.shape))
    print('mean.shape: ' + str(mean.shape))
    print('cov.shape: ' + str(cov.shape))
    print('np.linalg.det(cov): ' + str(np.linalg.det(cov)))
    sample = np.random.randn(3, )
    print('sample.shape: ' + str(sample.shape))
    Md = utils.squared_mahalanobis_distance(sample, mean, cov)
    print('Md: ' + str(Md))
    # mahalanobis without pseudo inverse
    x_dev = sample - mean
    S_inv = np.linalg.inv(cov)
    Md_wrong = x_dev.T.dot(S_inv).dot(x_dev)
    print('Md_wrong: ' + str(Md_wrong))


def test_squared_mahalanobis_distance_singular():
    """
    """
    # create data (5 samples, 3D)
    independent_samples = np.random.randn(5, 3)
    independent_samples[:,2] = 1
    independent_samples[1,2] = 1
    mean = np.mean(independent_samples, axis=0)
    cov = np.cov(independent_samples.T)
    print('independent_samples.shape: ' + str(independent_samples.shape))
    print('mean.shape: ' + str(mean.shape))
    print('cov.shape: ' + str(cov.shape))
    print('np.linalg.det(cov): ' + str(np.linalg.det(cov)))
    sample = np.random.randn(3, )
    print('sample.shape: ' + str(sample.shape))
    Md = utils.squared_mahalanobis_distance(sample, mean, cov)
    print('Md: ' + str(Md))


def main():
    """
    MAIN
    """
    test_squared_mahalanobis_distance_colinear()
    test_squared_mahalanobis_distance_singular()





if __name__ == '__main__':
    main()