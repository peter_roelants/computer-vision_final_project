#####################################################################
# Test the PCA functionalities
#####################################################################

import numpy as np
from matplotlib import pyplot as plt

import read_data
import pca_model
import shape_transform
import show_data
import shape as shape_utils
from shape import Shape


def plot_eigenvalues_var(eigenvalues):
    """
    Plot the relative cumulative variance of the given eigenvalues.
    Input:
        eigenvalues: eigenvalues to plot the cumulative variance of
        eigval.shape: (28,)
    Effect:
        Plot of the relative cumulative variance
    """
    # Calculate the relative cumulative sum
    eigval_sum = np.sum(eigenvalues)
    eigval_norm = np.divide(eigenvalues, eigval_sum)
    eigval_cumsum = np.cumsum(eigval_norm) * 100
    print 'eigval_cumsum: ', eigval_cumsum
    x = range(1,29)
    # left = [xi - 0.5 for xi in x]
    plt.bar(left=x, height=eigval_cumsum, width=0.8)
    # plt.xticks(x)
    plt.axhline(99, color='r')
    # plt.plot(eigval_cumsum)
    plt.grid(True)
    plt.ylim([65,100])
    plt.xlim([1,29])
    plt.ylabel('Cumulative variance (%)')
    plt.xlabel('Principal component')
    plt.title('Cumulative variance of the principal components')
    plt.show()

def plot_variance_components():
    """
    Plot the relative cumulative variance.
    """
    # shapes_imgs = read_data.get_all_8_teeth_imgs()
    # shapes_imgs = read_data.get_all_lower_teeth_imgs()
    # shapes_imgs = read_data.get_all_upper_teeth_imgs()
    shapes_imgs = read_data.get_all_segment_imgs(1)
    shapes, _ = zip(*shapes_imgs)
    eigenvalues, eigenvectors, mean_shape = pca_model.pca_of_shapes(shapes, 28)
    plot_eigenvalues_var(eigenvalues)


def plot_compare_bs(shapes, eigenvectors, b1, b2):
    """
    Compare the principal component parameters with each other. Plot the
    PC parameters so dependencies could be found.
    Input:
        shapes: list of shapes
        eigenvectors: eigenvectors of the shapes
        b1: int, index of first PC to test
        b2: int, index of second PC to test
    """  
    mean_shape = shape_utils.mean_shape(shapes)
    b1b2 = []
    for shape in shapes:
        projection = pca_model.project_shape(shape, eigenvectors, mean_shape)
        b1b2.append((projection[b1], projection[b2]))
    # plot b1 vs b2s
    (b1s, b2s) = zip(*b1b2)
    plt.plot(b1s, b2s, 'b+')
    plt.show()



def test_pca():
    """
    Test the pca functionality
    """
    shapes_imgs = read_data.get_all_8_teeth_imgs()
    shapes, _ = zip(*shapes_imgs)
    print 'type(shapes): ', type(shapes)
    print 'type(shapes[0]): ', type(shapes[0])
    # align all
    shapes = shape_transform.align_all_shapes(list(shapes))
    # do pca
    eigenvalues, eigenvectors, mean_shape = pca_model.pca_of_shapes(shapes)
    # plot mean shape
    xy_lim = 0.1
    ax = show_data.get_new_axis() 
    show_data.plot_shape_full(ax, mean_shape, 'k', ylim=[-xy_lim,xy_lim], xlim=[-xy_lim,xy_lim])
    plt.show()
    # plot eigenvalues
    print 'eigenvalues: ', eigenvalues
    plot_eigenvalues_var(eigenvalues)


def test_construct_reconstruct():
    """
    Test the pca reconstruct and construct functionality
    """
    shapes_imgs = read_data.get_all_8_teeth_imgs()
    shapes, _ = zip(*shapes_imgs)
    print 'type(shapes): ', type(shapes)
    print 'type(shapes[0]): ', type(shapes[0])
    # align all
    shapes = shape_transform.align_all_shapes(list(shapes))
    print 'len(shapes): ', len(shapes)
    # do pca
    eigenvalues, eigenvectors, mean_shape = pca_model.pca_of_shapes(shapes, 10)
    # Take a shape
    shape = shapes[0]
    # project shape
    b = pca_model.project_shape(shape, eigenvectors, mean_shape)
    print 'b: ', b
    # reconstruct shape
    reconstructed_shape = pca_model.reconstruct_shape(b, eigenvectors, mean_shape)
    # plot shapes
    xy_lim = 0.1
    ax = show_data.get_new_axis() 
    show_data.plot_shape_full(ax, shape, 'r', ylim=[-xy_lim,xy_lim], xlim=[-xy_lim,xy_lim])
    show_data.plot_shape_full(ax, mean_shape, 'k', ylim=[-xy_lim,xy_lim], xlim=[-xy_lim,xy_lim])
    show_data.plot_shape_full(ax, reconstructed_shape, 'g', ylim=[-xy_lim,xy_lim], xlim=[-xy_lim,xy_lim])
    plt.show()


def test_plot_compare_bs():
    """
    Plot the dependencies of the model parameters
    """
    shapes_imgs = read_data.get_all_8_teeth_imgs()
    shapes, _ = zip(*shapes_imgs)
    print 'type(shapes): ', type(shapes)
    print 'type(shapes[0]): ', type(shapes[0])
    # align all
    shapes = shape_transform.align_all_shapes(list(shapes))
    print 'len(shapes): ', len(shapes)
    # do pca
    eigenvalues, eigenvectors, mean_shape = pca_model.pca_of_shapes(shapes, 10)
    # plot
    plot_compare_bs(shapes, eigenvectors, 0, 1)


def test_get_cov_mean_all_models():
    """
    Plot the dependencies of the model parameters
    """
    shapes_imgs = read_data.get_all_8_teeth_imgs()
    shapes, _ = zip(*shapes_imgs)
    print 'type(shapes): ', type(shapes)
    print 'type(shapes[0]): ', type(shapes[0])
    # align all
    shapes = shape_transform.align_all_shapes(list(shapes))
    print 'len(shapes): ', len(shapes)
    # do pca
    eigenvalues, eigenvectors, mean_shape = pca_model.pca_of_shapes(shapes, 10)
    print 'eigenvalues.shape: ', eigenvalues.shape
    print 'eigenvectors.shape: ', eigenvectors.shape
    cov = pca_model.get_cov_mean_all_models(shapes, eigenvectors)
    print 'cov.shape: ', cov.shape



def plot_influence_pc(eigval, eigvec, mean_shape, pc_nb):
    """
    Plot the effect of changing the values of the pc_nb-th principal component.
    Input:
        eigval: eigenvalues
            eigval.shape: (shapes,)
        eigvec: eigenvectors belonging to the eigenvalues
            eigenvectors.shape: (points, shapes)
        mu: mean flat shape
            mu.shape: (points,)
        pc_nb: int, nb of the principal component
    """

    times_std_dev = 1
    # Get the standard deviance
    b_i = times_std_dev * np.sqrt(eigval[pc_nb])
    # Get the vector to multiply with the eigenvec
    b = np.zeros(eigval.shape, dtype=float)
    # Set the pc_nb-th element in the vector
    b[pc_nb] = b_i
    # Effect of +times_std_dev std dev
    s_plus = pca_model.reconstruct_shape(b, eigvec, mean_shape)
    s_plus.point_array[::2] = s_plus.point_array[::2] + 0.25
    # s_plus = Shape(s_plus.point_array + movement, s_plus.split_points)
    # Effect of -times_std_dev std dev
    s_min = pca_model.reconstruct_shape(-b, eigvec, mean_shape)
    s_min.point_array[::2] = s_min.point_array[::2] - 0.25
    # s_min = Shape(s_min.point_array - movement, s_min.split_points)
    # Plot
    ax = show_data.get_new_axis() 
    x_lim = 0.50
    y_lim = 0.40
    show_data.plot_shape_full(ax, mean_shape, 'k', ylim=[-y_lim,y_lim], xlim=[-x_lim,x_lim])
    show_data.plot_shape_full(ax, s_plus, 'r', ylim=[-y_lim,y_lim], xlim=[-x_lim,x_lim])
    show_data.plot_shape_full(ax, s_min, 'g', ylim=[-y_lim,y_lim], xlim=[-x_lim,x_lim])
    plt.show()




def test_plot_influence_pc(shapes_imgs):
    """
    Input:
        shapes_imgs: List of shapes and images [(shape, image), ...]
    The the plotting of influence of variance for the different PC's
    """
    # shapes_imgs = read_data.get_all_8_teeth_imgs()
    shapes, _ = zip(*shapes_imgs)
    print 'type(shapes): ', type(shapes)
    print 'type(shapes[0]): ', type(shapes[0])
    # align all
    shapes = shape_transform.align_all_shapes(list(shapes))
    print 'len(shapes): ', len(shapes)
    # do pca
    eigenvalues, eigenvectors, mean_shape = pca_model.pca_of_shapes(shapes, 10)
    # plot
    plot_influence_pc(eigenvalues, eigenvectors, mean_shape, 2)


def main():
    """
    MAIN
    """
    # test_pca()
    # test_construct_reconstruct()

    # plot_variance_components()

    # test_plot_compare_bs()
    # test_get_cov_mean_all_models()

    # shapes_imgs = read_data.get_all_8_teeth_imgs()
    # shapes_imgs = read_data.get_all_upper_teeth_imgs()
    # shapes_imgs = read_data.get_all_lower_teeth_imgs()
    shapes_imgs = read_data.get_all_segment_imgs(2)
    test_plot_influence_pc(shapes_imgs)
    





if __name__ == '__main__':
    main()