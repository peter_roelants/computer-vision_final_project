#####################################################################
# All things filters
#####################################################################

import cv2
import numpy as np

def filter_shape_img_list(shape_img_list, filter_fun):
    """
    Filter the given list of shapes and images [(shape, img)] by the given filter_fun.
    """
    nb_of_imgs = len(shape_img_list)
    new_list = [None] * nb_of_imgs
    for idx, (shape, img) in enumerate(shape_img_list):
        img = filter_fun(img)
        new_list[idx] = (shape, img)
    return new_list


def filter_function(img):
    """
    """
    img = substractBands2(img)
    return filtersSobels(img)



def filtersVincent(img):
    """
    Apply some filters to the image
    """
    img = substractBands2(img)
    # # blur

    img = cv2.GaussianBlur(img,(0,0),3)

    # # #clahe
    clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(20,20))
    img = clahe.apply(img)


    # # # hats 
    img = extractHat(img, 170,20)
    return img


def filtersSobels(img):
    img = filtersVincent(img)
    img = sobels(img)
    img = extractHat(img, 170,120)
    return img


# filter utilities


def sobels(img):
    size = 3
    dx = (cv2.Sobel(img, cv2.CV_16S,1,0, ksize = size) + 255)/4
    dy = (cv2.Sobel(img, cv2.CV_16S,0,1, ksize = size) + 255)/4

    img = cv2.add(dx,dy, cv2.CV_8U)
    img = np.uint8(np.absolute(img))
    return img


def extractHat(img, topr, bottomr):
    return cv2.subtract(cv2.add(img,topHat(img, topr)),bottomHat(img, bottomr))
def topHat(img, r):
    img = hatTransform(img, cv2.MORPH_TOPHAT,r)
    return img
def bottomHat(img, r):
    img = hatTransform(img, cv2.MORPH_BLACKHAT,r)
    return img
def hatTransform(img, op, r):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (r,r))
    img = cv2.morphologyEx(img, op, kernel)
    return img



def substractBands(img):
    offsettop = 50  # don't look at that black arrow
    (h,w) = img.shape
    # for x in xrange(0,w):
    for x in xrange(1200,1800):
        row = img[offsettop:,x]
        m = np.amin(row)
        img[offsettop:,x] = row - m
    return img

def substractBands2(img):
    offsettop = 50  # don't look at that black arrow
    blur = cv2.GaussianBlur(img,(0,0),3)
    profile = np.zeros(img.shape)
    (h,w) = img.shape
    # for x in xrange(0,w):
    for x in xrange(1200,1800):
        row = blur[offsettop:,x]
        m = np.mean(row)/5
        profile[:,x] = m
    
    img = cv2.subtract(img, profile, dtype=cv2.CV_8U)
    return img




### TEST


def testOutput(file_nb):
    img_name = '../../_Data/Radiographs/{:0>2d}.tif'.format(file_nb)
    img = cv2.imread(img_name,cv2.CV_LOAD_IMAGE_GRAYSCALE )



    img = filter_function(img)

    # crop
    img = img[500:1400, 1200:1800]
    file_name_out = 'out{:0>2d}.tif'.format(file_nb)
    cv2.imwrite(file_name_out,img)




def reportOutput(file_nb):
    img_name = '../../_Data/Radiographs/{:0>2d}.tif'.format(file_nb)
    img = cv2.imread(img_name,cv2.CV_LOAD_IMAGE_GRAYSCALE )


    file_name_out = '{:0>2d}.tif'.format(file_nb)
    cv2.imwrite(file_name_out,img[500:1400, 1200:1800])

    #bands
    img = substractBands2(img)

    # crop
    img = img[500:1400, 1200:1800]

    file_name_out = '{:0>2d}a.tif'.format(file_nb)
    cv2.imwrite(file_name_out,img)

    #blur
    img = cv2.GaussianBlur(img,(0,0),3)

    # clahe
    clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(20,20))
    img = clahe.apply(img)

    file_name_out = '{:0>2d}b.tif'.format(file_nb)
    cv2.imwrite(file_name_out,img)

    # hats
    img = extractHat(img, 170,20)

    file_name_out = '{:0>2d}c.tif'.format(file_nb)
    cv2.imwrite(file_name_out,img)

    # sobels
    img = sobels(img)

    file_name_out = '{:0>2d}d.tif'.format(file_nb)
    cv2.imwrite(file_name_out,img)

    # more hats
    img = extractHat(img, 170,120)

    file_name_out = '{:0>2d}e.tif'.format(file_nb)
    cv2.imwrite(file_name_out,img)

def main():
    for i in xrange(1,15):
        print("Proc img: " + str(i))
        reportOutput(i)

if __name__ == '__main__':
    main()