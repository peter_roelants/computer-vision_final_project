#####################################################################
# Active shape model
#####################################################################

import pca_model
import shape_transform
import utils



def update_shape_model(model_params, mean_shape, eigvecs, eigvals, target_shape):
    """
    Update the shape model in one iteration.
    Input:
        model_params: principal component parameters of the model
        mean_shape: shape
        eigvecs: eigenvectors
        target_shape: points to approximate (flat shape)
    Note:
        The mean_shape and eigenvectors need to be the result from the PCA where the norm
        of mean_shape == 1.
    Output:
        b: principal component parameters new model
        (tx, ty): x,y translation from mean
        s: scale from mean
        theta: rotation from mean
    """
    # Generate the model shape
    model_shape = pca_model.reconstruct_shape(model_params, eigvecs, mean_shape)
    # Find the pose paramaters which best align the model points to the target_points
    (tx, ty), s, theta = shape_transform.calc_transform_params(model_shape, target_shape)
    # project target_points into the model co-ordiante fram by inverting the transformation T
    y_shape = target_shape.inverse_transform(tx, ty, s, theta)
    # Project y into the tangent plane to mu by scaling
    y_shape = y_shape.scale(1.0/y_shape.distance(mean_shape))
    # Update the model parameters to match to y
    b = pca_model.project_shape(y_shape, eigvecs, mean_shape)
    return b, (tx, ty), s, theta


def update_shape_model_bounded(model_params, mean_shape, eigvecs, eigvals, target_shape, cov, mean_b):
    """
        Update the shape model in one iteration.
    Input:
        model_params: principal component parameters of the model
        mean_shape: mean shape
        eigvecs: eigenvectors
        target_shape: shape to approximate
        cov: covariance of bs
        mean_b: mean model
    Note:
        The mu and eigenvectors need to be the result from the PCA where the norm
        of mu == 1.
    Output:
        b: principal component parameters new model (limited by cutoff from Chi-square)
        (tx, ty): x,y translation from mean
        s: scale from mean
        theta: rotation from mean
    """
    b, (tx, ty), s, theta = update_shape_model(model_params, mean_shape, eigvecs, eigvals, target_shape)
    Md = utils.squared_mahalanobis_distance(b, mean_b, cov)
    cutoff = utils.chi_square_value(b.shape[0])
    print 'b.shape', b.shape
    if Md > cutoff:
        b = b * (cutoff / Md)
    return b, (tx, ty), s, theta



