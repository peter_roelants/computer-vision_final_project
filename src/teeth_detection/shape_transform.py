#####################################################################
# Transformation and alignment methods for shapes.
#####################################################################

import numpy as np

import shape as shape_util



def align_all_shapes(shapes):
    """
    Align all shapes with the help of procrustes analysis.
    Input:
        list of shapes
    Output:
        list of aligned shapes
    """
    # 1. Translate each shape so it's centroid is at the origin
    for idx, shape in enumerate(shapes):
        shapes[idx] = shape.move_to_origin()
    # 2. Calulate mean shape
    mean_shape = shape_util.mean_shape(shapes)
    # 3. Normalize orientation, scale, and origin of the current mean
    mean_shape = mean_shape.normalize()
    is_converged = False
    # Iterate untill convergence
    while not is_converged:
        # 4. Realign every shape with mean_shape
        for idx,shape in enumerate(shapes):
            (_, _), s, theta = calc_transform_params(shape, mean_shape)
            shapes[idx] = shape.transform(0, 0, s, theta)
        # 5. Calulate new mean shape
        prev_mean = mean_shape
        mean_shape = shape_util.mean_shape(shapes)
        # 6. Normalize orientation, scale, and origin of the current mean
        mean_shape = mean_shape.normalize()
        # Check convergence
        dist = mean_shape.distance(prev_mean)
        if 1-10**-15 < dist < 1+ 10**-15:
            is_converged = True
    # normalise to mean of 1
    mean_shape = shape_util.mean_shape(shapes)
    mean_shape_norm = mean_shape.norm()
    for idx,shape in enumerate(shapes):
        shapes[idx] = shape.scale(1.0/mean_shape_norm)
    return shapes


def calc_transform_params(shape_from, shape_to):
    """
    Calculate the transformation paramters to transform shape_from to shape_to.
    Input:
        shape_from: shape
        shape_to: shape
    Output:
        (tx, ty): x and y translation
        s : scale
        theta : rotation angle
    """
    # copy shapes
    x1 = shape_from.xy_2D()
    x2 = shape_to.xy_2D()
    # calculate the centroids
    centroid_x1 = np.mean(x1, axis=0)
    centroid_x2 = np.mean(x2, axis=0)
    (tx, ty) = centroid_x2 - centroid_x1
    # center the shapes around their centroids
    x1 = x1 - centroid_x1
    x2 = x2 - centroid_x2
    # calculate the optimal rotation and scale
    x1_norm_squared = np.linalg.norm(x1.flatten())**2
    a = np.dot(x1.flatten(), x2.flatten()) / x1_norm_squared
    b = np.sum(
        np.multiply(x1[:,0],x2[:,1]) - 
        np.multiply(x1[:,1],x2[:,0])) / x1_norm_squared
    # s is the factor to scale by
    s = np.sqrt(a**2 + b**2)
    # theta is the angle to rotate by
    theta = np.arctan(b/a)
    return (tx, ty), s, theta


