#####################################################################
# Main file that combines everything
#####################################################################

import read_data
import sample_point
import pca_model
import shape_transform
import filters
import initialization_params
import active_shape_model
import show_data
import utils

from shape import Shape






def run_detection_all_tooth_at_once():
    """
    """
    dist_file_name = "./img/all_teeth_dists.txt"
    with open(dist_file_name, "wb") as dist_file:
        dist_file.write("run_detection_all_tooth_at_once\n")
    nb_of_pc = 12
    test_sample_nbs = [1,2,3]
    # read training samples
    shape_img_list_train = read_data.get_all_8_teeth_imgs_except(test_sample_nbs)
    # perform crossvalidation for each radiograph
    for file_nb in test_sample_nbs:
        # Get the shape and image of the radiograph to test
        original_shape, original_img = read_data.get_shape_img(file_nb, False)
        found_shape = run_detection(original_img, shape_img_list_train, nb_of_pc, file_nb, 0)
        # plot binary image
        file_name = './img/all_teeth_{:0>2d}.png'.format(file_nb)
        show_data.write_binary_image(original_img, found_shape, file_name)
        # plot shape on image
        file_name = './img/comparison_all_teeth_{:0>2d}.png'.format(file_nb)
        show_data.plot_shape_on_img(found_shape, original_img, file_name)
        # get distances
        mean_dist = utils.mean_euclidian_dist(found_shape, original_shape)
        max_dist = utils.max_euclidian_dist(found_shape, original_shape)
        with open(dist_file_name, "a") as dist_file:
            dist_file.write('mean_dist: ' + str(mean_dist))
            dist_file.write('\n')
            dist_file.write('max_dist: ' + str(max_dist))
            dist_file.write('\n')


def run_detection_lower_upper():
    """
    """
    dist_file_name = "./img/lower_upper_teeth_dists.txt"
    with open(dist_file_name, "wb") as dist_file:
        dist_file.write("run_detection_lower_upper\n")
    nb_of_pc_lower = 10
    nb_of_pc_upper = 8
    test_sample_nbs = [1,2,3]
    # read training samples
    shape_img_list_lower = read_data.get_all_lower_teeth_imgs_except(test_sample_nbs)
    shape_img_list_lower = filters.filter_shape_img_list(shape_img_list_lower, filters.filter_function)
    shape_img_list_upper = read_data.get_all_upper_teeth_imgs_except(test_sample_nbs)
    shape_img_list_upper = filters.filter_shape_img_list(shape_img_list_upper, filters.filter_function)
    # perform crossvalidation for each radiograph
    for file_nb in test_sample_nbs:
        print ''
        print 'run_detection_all_lower_upper file_nb: ', file_nb

        # find the best match of the lower teeth
        original_shape_lower, original_img_lower = read_data.get_lower_teeth_img(file_nb, False)
        found_shape_lower = run_detection(original_img_lower, shape_img_list_lower, nb_of_pc_lower, file_nb, 0)

        # find the best match of the upper teeth
        original_shape_upper, original_img_upper = read_data.get_upper_teeth_img(file_nb, False)
        found_shape_upper = run_detection(original_img_upper, shape_img_list_upper, nb_of_pc_upper, file_nb, 1)

        # merged shape
        found_shape = Shape()
        found_shape.merge(found_shape_lower)
        found_shape.merge(found_shape_upper)

        original_merge_shape = Shape()
        original_merge_shape.merge(original_shape_lower)
        original_merge_shape.merge(original_shape_upper)

        # plot binary image
        file_name = './img/lower_upper_teeth_{:0>2d}.png'.format(file_nb)
        show_data.write_binary_image(original_img_lower, found_shape, file_name)
        # plot shape on image
        file_name = './img/comparison_lower_upper_teeth_{:0>2d}.png'.format(file_nb)
        show_data.plot_shape_on_img(found_shape, original_img_lower, file_name)
        # get distances
        mean_dist = utils.mean_euclidian_dist(found_shape, original_merge_shape)
        max_dist = utils.max_euclidian_dist(found_shape, original_merge_shape)
        with open(dist_file_name, "a") as dist_file:
            dist_file.write('mean_dist: ' + str(mean_dist))
            dist_file.write('\n')
            dist_file.write('max_dist: ' + str(max_dist))
            dist_file.write('\n')



def run_detection_segments():
    """
    """
    dist_file_name = "./img/segment_dists.txt"
    with open(dist_file_name, "wb") as dist_file:
        dist_file.write("run_detection_all_segments_seperate\n")
    nb_of_pc = 4
    test_sample_nbs = [1,2,3]
    # read training samples
    list_of_shapes_imgs = [None] * 8
    for seg_nb in xrange(1,9):
        shapes_imgs = read_data.get_all_segment_imgs_except(seg_nb, test_sample_nbs)
        list_of_shapes_imgs[seg_nb-1] = filters.filter_shape_img_list(shapes_imgs, filters.filter_function)
    # perform crossvalidation for each radiograph
    for file_nb in test_sample_nbs:
        print ''
        print 'run_detection_segments file_nb: ', file_nb
        original_img = read_data.get_img(file_nb, False)
        # run for each segment
        found_shape = Shape()
        original_merge_shape = Shape()
        for seg_nb in xrange(1,9):
            # get the shape and img to match
            original_segment = read_data.get_landmarks_for_segment(file_nb, seg_nb, False)
            # Get training data
            train_shapes_imgs = list_of_shapes_imgs[seg_nb-1]
            # find the best match
            found_segment = run_detection(original_img, train_shapes_imgs, nb_of_pc, file_nb, seg_nb)

            # merged shape
            found_shape.merge(found_segment)
            original_merge_shape.merge(original_segment)

        # plot binary image
        file_name = './img/segment_teeth_{:0>2d}.png'.format(file_nb)
        show_data.write_binary_image(original_img, found_shape, file_name)
        # plot shape on image
        file_name = './img/comparison_segment_teeth_{:0>2d}.png'.format(file_nb)
        show_data.plot_shape_on_img(found_shape, original_img, file_name)
        # get distances
        mean_dist = utils.mean_euclidian_dist(found_shape, original_merge_shape)
        max_dist = utils.max_euclidian_dist(found_shape, original_merge_shape)
        with open(dist_file_name, "a") as dist_file:
            dist_file.write('dists for file: ' + str(file_nb))
            dist_file.write('\n')
            dist_file.write('mean_dist: ' + str(mean_dist))
            dist_file.write('\n')
            dist_file.write('max_dist: ' + str(max_dist))
            dist_file.write('\n')


def run_detection(original_img, shape_img_list, nb_of_pc, file_nb, seg_nb):
    """
    """
    # define parameters
    k = 25 # nb of points in mean line
    m = 44  # nb of points in sample
    step_size = 1.5 # for sampling
    nb_of_iterations = 301
    normalize_fun = utils.normalize_min_max

    # filter image to test
    filtered_img = filters.filter_function(original_img)

    # build model of lines around each point in the training set
    point_models = sample_point.build_shape_point_lines_model(shape_img_list, k, step_size, normalize_fun)
    mean_lines, _ = zip(*point_models)
    # build shape model of training set
    original_shapes, _ = zip(*shape_img_list)
    shapes = shape_transform.align_all_shapes(list(original_shapes))
    eigenvalues, eigenvectors, mean_shape = pca_model.pca_of_shapes(shapes, nb_of_pc)
    B_cov = pca_model.get_cov_mean_all_models(shapes, eigenvectors)
    
    # get rotation and scale limits
    (theta_min, theta_plus), (s_min, s_max) = initialization_params.get_rotation_and_scale_limits(original_shapes, mean_shape)
    # get initial position
    (x_init, avg_y_diff), s_init = initialization_params.calculate_initial_transform_with_img(shape_img_list, mean_shape)
    # best mid
    y_mid = initialization_params.findMid(original_img)
    y_init = y_mid + avg_y_diff
    print '(x_init, y_init), s_init, avg_y_diff: ', ((x_init, y_init), s_init, avg_y_diff)

    # initial shape to start search for best fit
    current_shape =  mean_shape.transform(x_init, y_init, s_init, 0)

    # make the initial model
    b = pca_model.get_empty_model(nb_of_pc)
    mean_b = pca_model.get_empty_model(nb_of_pc)

    # hold the data while iterating
    shapes_iterations = [current_shape]
    # iterate until convergence
    for i in xrange(nb_of_iterations):
        print 'iteration: ', i
        # sample the lines
        samples = sample_point.sample_line_full_shape(filtered_img, current_shape, m, step_size, normalize_fun)

        # get new points
        point_movements = sample_point.compare_figure_lines(point_models, samples, step_size, normalize_fun)
        (movements, posititions, normals, mds) = zip(*point_movements)
        new_shape = sample_point.get_new_points(point_movements, step_size, mean_shape)
        # get tranformation
        b, (tx, ty), s, theta = active_shape_model.update_shape_model_bounded(b, mean_shape, eigenvectors, eigenvalues, new_shape, B_cov, mean_b)
        # limit theta:
        if not (theta_min <= theta <= theta_plus):
            theta = 0
        # limit zoom:
        if s < s_min:
            s = s_min
        if s > s_max:
            s = s_max
        print('b, (tx, ty), s, theta: ' + str((b, (tx, ty), s, theta)))
        # construct the found shape
        current_shape = pca_model.reconstruct_shape(b, eigenvectors, mean_shape)
        current_shape = current_shape.transform(tx, ty, s, theta)

        if i % 50 == 0:
            show_data.plot_movements(shapes_iterations[-1], new_shape, filtered_img, file_nb, i, seg_nb)


        # sample data for plotting
        shapes_iterations.append(current_shape)



    # return the found shape
    return current_shape






def main():
    """
    MAIN
    """
    run_detection_segments()
    run_detection_all_tooth_at_once()
    run_detection_lower_upper()


if __name__ == '__main__':
    main()