#####################################################################
# Initialisation params
#####################################################################

import cv2
import numpy as np

import shape_transform
import shape as shape_utils

def get_ellipse_mask(img, centerX, centerY, width, height):
    '''
    Get a mask for the given image of an ellipse.
    '''
    mask = np.zeros(img.shape , np.uint8)
    cv2.ellipse(
        mask, # Image where the circle is drawn.
        (centerX,centerY), # center: (centerX,centerY), 
        (width,height), # axes: (width,height), 
        0, # angle
        0, # startAngle
        360, # endAngle
        cv2.cv.CV_RGB(255, 255, 255), # color
        -1 # Thickness of the ellipse outline, if positive. Negative thickness means that a filled circle is to be drawn.
        )
    return mask


def findMid(img):
    """
    Find the midpoint of the image on the y-axis.
    """
    # subtract bounds:
    (h,w) = img.shape
    y_lim = (700, h-400)
    x_lim = (1200, w-1200)
    cut_img = img[y_lim[0]:y_lim[1], x_lim[0]:x_lim[1]]
    # get mask:
    (h,w) = cut_img.shape
    besty = 0
    bestyVal = 255
    for y in xrange(0,h):
        mask = get_ellipse_mask(cut_img, w/2, y, 200, 20)
        (val,_,_,_) = cv2.mean(cut_img, mask=mask)
        if val < bestyVal:
            besty = y
            bestyVal = val
    return besty + y_lim[0]


def get_rotation_and_scale_limits(original_shapes, mu):
    """
    Input:
        original_shapes: Original shapes
        mu: mean model
    Output:
        (min_theta, max_theta): the rotation limits
    """
    thetas = []
    ss = []
    for shape in original_shapes:
        (_, _), s, theta = shape_transform.calc_transform_params(mu, shape)
        thetas.append(theta)
        ss.append(s)
    return (min(thetas), max(thetas)), (min(ss), max(ss))


def calculate_initial_transform(original_shapes, mu):
    """
    Calculate the initial transformation to be made from the original shapes.
    Input:
        original_shapes: Original shapes before any preprocessing
        mu: the mean shape to start from
    Output: (x, y), s
        (x, y): translation
        s : scale
    """
    # Get the mean shape of the unmirrored shapes
    mean_shape = shape_utils.mean_shape(original_shapes)
    x = np.mean(mean_shape.xs(), axis=0)
    y = np.mean(mean_shape.ys(), axis=0)
    (_, _), s, _ = shape_transform.calc_transform_params(mu, mean_shape)
    return (x, y), s


def calculate_initial_transform_with_img(original_shape_img_list, mu):
    """
    Calculate the initial transformation to be made from the original shapes.
    Input:
        original_shape_img_list: list of (shape, img)
        mu: the mean shape to start from
    Output: (x, y), s
        (x, y): translation
        s : scale
    """
    avg_y_diff = 0
    for (shape, img) in original_shape_img_list:
        y_mid_img = findMid(img)
        y_mid_shape = shape.centroid()[0,1]
        avg_y_diff += (y_mid_shape - y_mid_img)
    avg_y_diff = avg_y_diff / len(original_shape_img_list)

    shapes, _ = zip(*original_shape_img_list)

    # Get the mean shape of the unmirrored shapes
    mean_shape = shape_utils.mean_shape(shapes)
    x = np.mean(mean_shape.xs(), axis=0)
    (_, _), s, _ = shape_transform.calc_transform_params(mu, mean_shape)
    return (x, avg_y_diff), s



