#####################################################################
# This file contains methods to show shapes and figures
#####################################################################

from matplotlib import pyplot as plt
import matplotlib.path as mpath
import matplotlib.patches as mpatches

import read_data

import numpy as np
import cv2


def plot_shape_indexes(ax, shape, color):
    """
    Plot the given shape with the point index numbers on the given axis.
    """
    for i in xrange(shape.nb_of_points()):
        ax.plot(shape.x(i), shape.y(i), marker=r'$' + str(i) + '$', 
                markerfacecolor=color , markeredgecolor=color)


def plot_shape_points(ax, shape, marker):
    """
    Plot the given shape as points on the given axis.
    """
    ax.plot(shape.xs(), shape.ys(), marker)


def plot_xy_points(ax, x, y, marker):
    """
    Plot the given shape as points on the given axis.
    """
    ax.plot(x, y, marker)


def plot_shape_full(ax, shape, color, ylim=[500,1400], xlim=[1200,1800]):
    """
    Plot the given shape as points on the given axis.
    """
    shapes = shape.split()
    for s in shapes:
        Path = mpath.Path
        path_data = [(Path.MOVETO, (s.x(0), s.y(0))),]
        # Add all tooth points
        for i in xrange(1,s.nb_of_points()):
            path_data.append((Path.LINETO, (s.x(i), s.y(i))))
        # Close the tooth shape
        path_data.append((Path.CLOSEPOLY, 
            (s.x(s.nb_of_points()-1), s.y(s.nb_of_points()-1))))
        # Create the patch for the tooth and plot it on the given axis
        codes, verts = zip(*path_data)
        path = mpath.Path(verts, codes)
        patch = mpatches.PathPatch(path, facecolor=color, alpha=0.25)
        ax.add_patch(patch)
        plt.ylim(ylim)
        plt.xlim(xlim)


def plot_img(ax, img):
    """
    Plot the given image on the given axis.
    """
    ax.imshow(img, cmap = 'gray', interpolation = 'bicubic')


def plot_all_teeth_on_img():
    """
    Plot all shapes on all images
    """
    shapes_imgs = read_data.get_all_8_teeth_imgs()
    for idx, (shape, img) in enumerate(shapes_imgs):
        ax = get_new_axis()
        plot_img(ax, img)
        plot_shape_points(ax, shape, 'r+')
        plt.ylim([500,1400])
        plt.xlim([1200,1800])
        # save image
        file_name = './img/shape'
        if idx < 14:
            file_name = file_name + '_{:0>2d}.png'.format(idx+1)
        else:
            file_name = file_name + '_{:0>2d}_mirror.png'.format(idx-13)
        print('plot: ' + str(file_name))
        plt.savefig(file_name, dpi=300, bbox_inches='tight', pad_inches=0)


def plot_mean_lines_vs_sample(mean, samples, movement, m_dists):
    """
    Plot the mean lines vs the sample lines (samples) together with the movements 
    and mahalanobis distances (m_dists) for each point.
    """
    nb_of_lines = len(mean)
    print('len(mean): ' + str(len(mean)))
    print('len(samples): ' + str(len(samples)))
    print('len(movement): ' + str(len(movement)))
    sample_length = samples[0].shape[0]
    print('sample_length: ' + str(sample_length))
    mean_img = np.ndarray(shape=(len(mean), mean[0].shape[0]))
    for p_idx in xrange(len(mean)):
        mean_img[p_idx] = mean[p_idx][:]
    samples_img = np.ndarray(shape=(len(samples), samples[0].shape[0]))
    for p_idx in xrange(len(samples)):
        samples_img[p_idx] = samples[p_idx][:]
    fig = plt.figure()
    ax = fig.add_subplot(1,3,1)
    ax.imshow(mean_img, cmap = 'gray', interpolation = 'nearest')
    plt.ylim([0,nb_of_lines])
    ax = fig.add_subplot(1,3,2)
    ax.imshow(samples_img, cmap = 'gray', interpolation = 'nearest')
    for p_idx in xrange(len(movement)):
        ax.plot((sample_length/2) + movement[p_idx], p_idx, 'r+' )
    plt.ylim([0,nb_of_lines])
    ax = fig.add_subplot(1,3,3)
    for p_idx in xrange(len(m_dists)):
        ax.plot(m_dists[p_idx], p_idx, 'ro' )
    plt.ylim([0,nb_of_lines])
    plt.show()


def plot_movements(shape_from, shape_to, img, file_nb, iteration, seg_nb):
    """
    Plot the movement of the shape_from to shape_to on the given img.
    """
    ax = get_new_axis()
    plot_img(ax, img)
    for p_idx in xrange(shape_from.nb_of_points()):
        ax.plot([shape_from.x(p_idx), shape_to.x(p_idx)], [shape_from.y(p_idx), shape_to.y(p_idx)], 'r-')
        ax.plot(shape_from.x(p_idx), shape_from.y(p_idx), 'b+')
    plt.ylim([500,1400])
    plt.xlim([1200,1800])
    file_name = './img/movement_{:0>2d}_{:0>1d}_{:0>3d}.png'.format(file_nb, seg_nb, iteration)
    plt.savefig(file_name, dpi=300, bbox_inches='tight', pad_inches=0)


def plot_shape_on_img(shape, img, file_name):
    """
    Plot the given shape on the given image and write to the given file_name.
    """
    ax = get_new_axis()
    plot_img(ax, img)
    plot_shape_full(ax, shape, 'b')
    plt.ylim([500,1400])
    plt.xlim([1200,1800])
    plt.savefig(file_name, dpi=300, bbox_inches='tight', pad_inches=0)


def get_new_axis():
    """
    Return a new axis to plot on.
    """
    fig = plt.figure()
    return fig.add_subplot(1,1,1)



def write_binary_image(original_img, shape, file_name):
    """
    Write a binary image to show the teeth shapes.
    
    There is no warning for shapes that lay out of the image (e.g. mirrorred). 

    Input:
        original_img: (opencv array) reference for dimensions. ensures correct checking by overlaying.
        shape: a shape object. can consist of multiple teeth. 
        file_name: output destination.
    Effect:
        write the image on disk
    """
    out_img = np.zeros(original_img.shape)

    shapes = shape.split()

    for s in shapes:
        pts = s.xy_2D().astype('int32')
        cv2.fillConvexPoly(out_img, pts, color=256, lineType=4)
    print "Written", len(shapes), "shapes to", file_name
    cv2.imwrite(file_name,out_img)



def main():
    """
    MAIN
    """
    # shape = read_data.get_landmarks_for_segment(1,1,False)
    # ax = get_new_axis()
    # plot_img(ax, img)
    # plot_shape_full(ax, shape, 'r')
    # plt.ylim([500,1400])
    # plt.xlim([1200,1800])
    # plt.show()
    # plot_all_teeth_on_img()

    shape = read_data.get_8_teeth(1, False)
    img = read_data.get_img(1, False)
    write_binary_image(img, shape, "bin.tif")

if __name__ == '__main__':
    main()