#####################################################################
# Sampling of point information on image
#####################################################################

import numpy as np

import utils
from shape import Shape



#####################################################################
# Get normal of points
#####################################################################
def getNormal(prev,point,next):
    """
    given a segment of a shape, return the normal pointing outwards
    Input:
        prev: 2d points
        point: 2d points
        next: 2d points
    """
    a = prev
    b = point
    c = next
    # vectors a->b , b->c
    ab = utils.normalize(b - a)
    bc = utils.normalize(c - b)
    # rotate 90 deg for segment normal
    ab[1] = -ab[1]
    bc[1] = -bc[1]
    ab = ab[::-1]
    bc = bc[::-1]
    # avg normal of segment normals
    n = utils.normalize(ab + bc)
    return n


def get_normals(shape):
    """
    Return the normals for each point in the shape
    """
    nb_of_points = shape.nb_of_points()
    # make a list of 2D points with a wrap around
    points = shape.xy_2D()
    points = np.vstack((points[nb_of_points-1,:], points, points[0:]))
    # wrap_points = np.hstack(points[1,:], points, points[-1:])
    points_normals = [None] * nb_of_points
    for i in xrange(nb_of_points):
        # print('threePoints: ' + str(threePoints))
        normal = getNormal(points[i,:], points[i+1,:], points[i+2,:])
        points_normals[i] = (points[i+1,:], normal.T)
    return points_normals




#####################################################################
# Sample line
#####################################################################
def sample_line(img, startpoint, direction, k, step_size, normalize_fun=None):
    """
    Sample a line from the image around the starting point.
    Input:
        img: image to sample from
        startpoint: [px,py] middlepoint of image
        direction: [nx,ny] direction to sample in
            (sampling will be done on both sides of the point)
        k: number of samples on one side of the point
        step_size: size of the steps
        normalize_fun: normalization function
    Output:
        line: (ndarray) line.shape:  (k*2+1,)
    """
    px = startpoint[0]
    py = startpoint[1]
    nx = direction[0]
    ny = direction[1]
    line = np.ndarray(shape=(k*2+1))
    for i in xrange(k,0,-1):
        step = i * step_size
        (sx,sy) = (px + step * nx, py + step * ny)
        line[k-i] = img[sy,sx]
    line[k] = img[py,px]
    for i in xrange(1,k+1):
        step = i * step_size
        (sx,sy) = (px - step * nx, py - step * ny)
        line[k+i] = img[sy,sx]
    # normalize line
    if normalize_fun != None:
        line = normalize_fun(line)
    return line


def _sample_line_from_split_shape(img, shape, k, step_size, normalize_fun=None):
    """
    Sample the linse from the image of the given tooth points.
    Input:
        img: image to sample from
        shape: shape with points to sample from
        k: number of sample on one side of the point
        step_size: the number of steps to be taken each time
    Output:
        samples: List of tuples (sample, mid_point, normal)
            sample: ndarray (k*2+1,)
            mid_point: [px,py] point location
            direction: [nx,ny] direction of line
    """
    nb_of_points = shape.nb_of_points()
    points_normals = get_normals(shape)
    samples = [None] * nb_of_points
    for idx, (point, normal) in enumerate(points_normals):
        sample = sample_line(img, point, normal, k, step_size, normalize_fun)
        samples[idx] = (sample, point, normal)
    return samples


def sample_line_full_shape(img, shape, k, step_size, normalize_fun=None):
    """
    Sample lines of the given shape on the given figure.
    Input:
        img: figure to sample lines from
        shape: shape
        k: number of pixels to sample on each side of each point
        step_size: the number of steps to be taken each time
    Output:
        lines: List of tuples (line, mid_point, normal) sampled for each point
            len(lines): nb_of_points
            line: ndarray (k*2+1,)
            mid_point: (px,py) point location
            direction: (nx,ny) direction of line
    """
    shape_list = shape.split()
    all_samples = []
    for shape in shape_list:
        all_samples = all_samples + _sample_line_from_split_shape(img, shape, k, step_size, normalize_fun)
    return all_samples


def build_shape_point_lines_model(img_shape_list, k, stepsize, normalize_fun=None):
    """
    Input: 
        img_shape_list: List of shapes and images [(shape, img), ...]
    Output:
        point_models: List of tuples (mean, covariance) for each point
            mean.shape: (2*k+1,)
            covariance.shape: (2*k+1, 2*k+1)
    """
    nb_of_images = len(img_shape_list)
    nb_of_points = img_shape_list[0][0].nb_of_points()
    # initialise all_points
    all_points = [None] * nb_of_points
    for idx in xrange(nb_of_points):
        all_points[idx] = np.zeros((nb_of_images, 2*k+1))
    # Get all line samples
    for img_idx in xrange(nb_of_images):
        (shape, img) = img_shape_list[img_idx]
        lines = sample_line_full_shape(img, shape, k, stepsize, normalize_fun)
        for p_idx in xrange(nb_of_points):
            all_points[p_idx][img_idx] = lines[p_idx][0]
    # calculate the mean and covariance matrices
    point_models = [None] * nb_of_points
    for p_idx in xrange(nb_of_points):
        mean_line = np.mean(all_points[p_idx], axis=0)
        cov_line = np.cov(all_points[p_idx].T)
        point_models[p_idx] = (mean_line, cov_line)
    return point_models


def compare_line(mean, cov, new_sample, stepsize, normalize_fun=None):
    """
    Compare the mean sample to the new_sample to find the best translation.
    Input:
        mean: Mean line to compare with new_sample
            mean.shape = (2*k+1,)
        cov: covariance matrix corresponding to mean
            cov.shape = (2*k+1, 2*k+1)
        new_sample: Sample line to compare the mean on
            new_sample.shape = (2*m+1, )
        stepsize: stepsize to take
        normalize_fun: function to normalize each sample
    Output:
        Number of shifts the mean has to make from the center of new_sample
        that results in the best match
    """
    k = (mean.shape[0]-1)/2
    m = (new_sample.shape[0]-1)/2
    nb_of_shifts = 2*(m-k) + 1
    # Get the mahalanobis distances
    min_dist = float("inf")
    min_idx = 0
    for i in xrange(nb_of_shifts):
        compare_sample = new_sample[i:(2*k)+1+i]
        # normalize line
        if normalize_fun != None:
            compare_sample = normalize_fun(compare_sample)
        Md = utils.squared_mahalanobis_distance(compare_sample, mean, cov)
        if (Md < min_dist):
            min_dist = Md
            min_idx = i
    mid_point = (nb_of_shifts-1)/2
    movement = (mid_point - min_idx) * stepsize
    return movement, min_dist


def compare_figure_lines(point_models, samples, stepsize, normalize_fun=None):
    """
    Compare the figure in samples to the model in point_models.
    Input:
        point_models: List of tuples (mean, covariance) for each point
            mean.shape: (2*k+1,)
            covariance.shape: (2*k+1, 2*k+1)
        samples: List of tuples (line, mid_point, normal) sampled for each point
            len(samples): nb_of_points
            line: ndarray (k*2+1,)
            mid_point: [px,py] point location
            normal: [nx,ny] direction of line
        stepsize: step size to take
        normalize_fun: function to normalize each sample
    Output:
        point_movements: List of tuples(movement, positition, normal, md)
            len(point_movements): nb_of_points
            movement: movement in nb of pixels
            positition: (px,py) point location
            normal: (nx,ny) direction of line
            md: Mahalanobis distance fit
    """
    nb_of_points = len(point_models)
    point_movements = [None] * nb_of_points
    for p_idx in xrange(nb_of_points):
        mean_line = point_models[p_idx][0]
        cov = point_models[p_idx][1]
        line = samples[p_idx][0]
        movement, Md = compare_line(mean_line, cov, line, stepsize, normalize_fun)
        point_movements[p_idx] = (movement, samples[p_idx][1], samples[p_idx][2], Md)
    return point_movements



#####################################################################
# Get new point locations from movements
#####################################################################
def get_new_point(movement, startpoint, direction, step_size, Md):
    """
    Get a new pixel location following the movement in the direction
    starting from the startpoint.
    """
    px = startpoint[0]
    py = startpoint[1]
    nx = direction[0]
    ny = direction[1]
    new_x = px + nx * movement * step_size
    new_y = py + ny * movement * step_size
    return (new_x, new_y)


def get_new_points(point_movements, step_size, mean_shape):
    """
    """
    nb_of_points = len(point_movements)
    new_points = np.ndarray(shape=(1,nb_of_points*2))
    for i in xrange(nb_of_points):
        movement, startpoint, direction, Md = point_movements[i]
        (x, y) = get_new_point(movement, startpoint, direction, step_size, Md)
        new_points[0,i*2] = x
        new_points[0,i*2+1] = y
    return Shape(new_points, mean_shape.split_points)



