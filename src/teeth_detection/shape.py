#####################################################################
# This file contains the shape class that holds a shape
#####################################################################

import numpy as np


def is_flat(array):
    """
    Return true iff array is a flat array.
    """
    return len(array.shape) == 1


def mean_shape(shapes):
    """
    Get the mean of the given shapes:
    Input:
        list of shapes
    Output:
        mean shape
    """
    if not shapes:
        raise ValueError("mean: shapes list is empty.")
    nb_of_shapes = len(shapes)
    shape_size = shapes[0].size()
    shapes_matrix = np.ndarray(shape=(nb_of_shapes,shape_size))
    for idx,shape in enumerate(shapes):
        shapes_matrix[idx,:] = shape.xy_1D()
    mean_array = np.mean(shapes_matrix, axis=0)
    return Shape(mean_array, shapes[0].split_points)



class Shape:
    """
    Class to represent a shape of points.
    The internal point array is represented as a 1D numpy array.
        point_array[2*n] holds the x-pos for point n
        point_array[2*n+1] holds the y-pos for point n

    A shape can consist of multiple shapes (eg multiple teeth),
    the split points are in self.point_array
    """


    def __init__(self, point_array=None, split_points=None):
        """
        Initialise the shape with a point array.
        Input:
            point_array: numpy array of points
                if point_array.shape = (nb_of_points, 2) then
                    x-coordinates by shape[:,0]
                    y-coordinates by shape[:,1]
                if point.array.shape = (nb_of_points*2,) then 
                    point_array[2*n] holds the x-pos for point n
                    point_array[2*n+1] holds the y-pos for point n
            split_points: the points where to split the shape if it consists
                of multiple merges.
        """
        # If there is no point array, or the point array is empty
        # then the shape is empty
        if point_array == None or point_array.size == 0:
            self.point_array = np.array([])
            self.split_points = [0]
        # There is a nonempty point_array
        else:
            # assign point array
            if not is_flat(point_array):
                self.point_array = point_array.flatten()
            else:
                self.point_array = point_array.copy()
            # assign split_points
            if split_points == None or len(split_points) == 1:
                self.split_points = [0, self.point_array.shape[0]]
            else:
                self.split_points = split_points[:]
                # check if last elmenent is already in list
                last_element = self.point_array.shape[0]
                if self.split_points[-1] != last_element:
                    self.split_points.append(last_element)


    def nb_of_points(self):
        """
        Return the number of points in this shape.
        """
        return self.point_array.shape[0]/2


    def size(self):
        """
        Return the array size of this shape.
        """
        return self.point_array.shape[0]


    def x(self, idx):
        """
        Return the x-coordinate at the given idx.
        """
        return self.point_array[idx*2]


    def y(self, idx):
        """
        Return the y-coordinate at the given idx.
        """
        return self.point_array[idx*2+1]


    def xs(self):
        """
        Return the x x-coordinates of this shape.
        """
        return self.point_array[0::2]


    def ys(self):
        """
        Return the y-coordinates of this shape
        """
        return self.point_array[1::2]


    def xy_2D(self):
        """
        Return this shape as a 2D matrix.
        Output:
            matrix.shape = (nb_of_points, 2)
                x-coordinates in matrix[:,0]
                y-coordinates in matrix[:,1]
        """
        array = np.ndarray(shape=(self.nb_of_points(),2))
        array[:,0] = self.xs()
        array[:,1] = self.ys()
        return array


    def xy_1D(self):
        """
        Return this shape as a 1D matrix.
        Output:
            self.point_array
        """
        return self.point_array.copy()


    def norm(self):
        """
        Get the lin alg norm of this shape
        """
        return np.linalg.norm(self.point_array)


    def scale(self, scale):
        """
        """
        array = self.xy_1D()
        array = array * scale
        return Shape(array, self.split_points)


    def normalize(self):
        """
        Return the normalize shape
        """
        return self.scale(1.0/self.norm())


    def merge(self, other_shape):
        """
        Merge the other shape into this shape.
        Divisions of the other shape are not maintained.
        """
        if other_shape.point_array != None:
            translate_other_split_points = [p + self.size() for p in other_shape.split_points]
            self.split_points = self.split_points + translate_other_split_points[1:]
            # add other numpy array
            self.point_array = np.append(self.point_array, other_shape.point_array)
        # print 'merge: ', self.split_points


    def split(self):
        """
        Split the shape into multiple parts that were merged into it.
        """
        shape_list = []
        # Return an empty list if this shape is empty
        if self.point_array == None:
            return shape_list
        # return this shape in list if there are only 2 split points
        if len(self.split_points) == 2:
            return [self]
        # Else split the shape into shapes
        for idx in xrange(len(self.split_points)-1):
            start_idx = self.split_points[idx]
            end_idx = self.split_points[idx+1]
            shape_list.append(Shape(self.point_array[start_idx:end_idx]))
        return shape_list


    def centroid(self):
        """
        Return the centroid of this shape as a numpy array.
        centroid[0,0] = x-coordinate
        centroid[0,1] = y-coordinate
        """
        centroid = np.ndarray(shape=(1,2))
        centroid[0,0] = np.mean(self.xs(), axis=0)
        centroid[0,1] = np.mean(self.ys(), axis=0)
        return centroid


    def move_to_origin(self):
        """
        Return a new shape that has it centroid at the origin
        """
        centroid = self.centroid()
        array = self.point_array.copy()
        array[0::2] = array[0::2] - centroid[0,0]
        array[1::2] = array[1::2] - centroid[0,1]
        return Shape(array, self.split_points)


    def transform(self, tx, ty, s, theta):
        """
        Transform the this shape according to the given transformation parameters.
        Input:
            tx : x translation
            ty : y translation
            s : scale
            theta : rotation angle
        Return:
            new shape
        """
        # calculate the centroid
        centroid = self.centroid()
        # center the shape on the centroid
        shape_matrix = self.xy_2D() - centroid
        sc = s * np.cos(theta)
        ss = s * np.sin(theta)
        # R is the rotation and scale array
        R = np.array([[sc, -ss], [ss, sc]])
        new_shape_matrix = np.dot(R, shape_matrix.T).T + np.array([tx, ty]) + centroid
        return Shape(new_shape_matrix, self.split_points)


    def inverse_transform(self, tx, ty, s, theta):
        """
        Transform the this shape according to the inverse of the given 
            transformation parameters.
        Input:
            tx : x translation
            ty : y translation
            s : scale
            theta : rotation angle
        Return:
            new shape
        """
        return self.transform(-tx, -ty, 1.0/s, -theta)


    def distance(self, other_shape):
        """
        Return the distance of this shape with the other shape
        """
        return np.sqrt(self.point_array.T.dot(other_shape.point_array))


    def __str__(self):
        """
        toString
        """
        return 'Shape: ' + str(self.nb_of_points()) + ', ' + str(self.split_points) + ', ' + str(self.point_array)

