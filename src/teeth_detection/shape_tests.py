#####################################################################
# Test some shape functionalities
#####################################################################

from matplotlib import pyplot as plt
import numpy as np

import shape as shape_utils
from shape import Shape
import show_data
import shape_transform
import read_data



def test_visulize_transform():
    """
    Test the tranformation of shapes in a visual way.
    """
    # Create 2 squares
    square_matrix_1 = np.array(
        [[1, 1],
         [-1, 1],
         [-1, -1],
         [1, -1]])
    shape_1 = Shape(square_matrix_1)
    square_matrix_2 = np.array(
        [[4, 1],
         [2, 1],
         [2, -1],
         [4, -1]])
    shape_2 = Shape(square_matrix_2)
    # merge
    shape_1.merge(shape_2)
    shape = shape_1
    # transform
    tx = 1
    ty = 1
    s = 2
    theta = 0.785398163
    rotated_shape = shape.transform(tx, ty, s, theta)
    inv_rotated_shape = rotated_shape.inverse_transform(tx, ty, s, theta)
    print shape
    print rotated_shape
    print inv_rotated_shape
    xy_lim = 10
    # show
    ax = show_data.get_new_axis() 
    print 'show data: shape'
    show_data.plot_shape_full(ax, shape, 'k', ylim=[-xy_lim,xy_lim], xlim=[-xy_lim,xy_lim])
    print 'show data: rotated_shape'
    show_data.plot_shape_full(ax, rotated_shape, 'r', ylim=[-xy_lim,xy_lim], xlim=[-xy_lim,xy_lim])
    print 'show data: inv_rotated_shape'
    show_data.plot_shape_full(ax, inv_rotated_shape, 'g', ylim=[-xy_lim,xy_lim], xlim=[-xy_lim,xy_lim])
    plt.show()


def test_simple_calc_transform_params():
    """
    Test the calculation of transformation paramters
    """
     # Create 2 squares
    square_matrix = np.array(
        [[1, 1],
         [-1, 1],
         [-1, -1],
         [1, -1]])
    shape1 = Shape(square_matrix)
    shape2 = shape1.transform(2, 2, 3, 1)
    (tx, ty), s, theta = shape_transform.calc_transform_params(shape1, shape2)
    print '(tx, ty), s, theta: ', (tx, ty), s, theta
    (tx, ty), s, theta = shape_transform.calc_transform_params(shape2, shape1)
    print '(tx, ty), s, theta: ', (tx, ty), s, theta


def test_simple_align_all_shapes():
    """
    """
    # Create 2 squares
    square_matrix_1 = np.array(
        [[1, 1],
         [-1, 1],
         [-1, -1],
         [1, -1]])
    shape_1 = Shape(square_matrix_1)
    square_matrix_2 = np.array(
        [[4, 1],
         [2, 1],
         [2, -1],
         [4, -1]])
    shape_2 = Shape(square_matrix_2)
    # merge
    shape_1.merge(shape_2)
    shape_a = shape_1
    shape_b = shape_a.transform(2, 2, 3, 1)
    shapes = [shape_a, shape_b]
    # align all
    shapes = shape_transform.align_all_shapes(shapes)
    # show
    xy_lim = 1
    ax = show_data.get_new_axis()
    print shapes[0]
    show_data.plot_shape_full(ax, shapes[0], 'r', ylim=[-xy_lim,xy_lim], xlim=[-xy_lim,xy_lim])
    print shapes[1]
    show_data.plot_shape_full(ax, shapes[1], 'r', ylim=[-xy_lim,xy_lim], xlim=[-xy_lim,xy_lim])
    plt.show()


def test_teeth_align_all_shapes():
    """
    """
    shapes_imgs = read_data.get_all_8_teeth_imgs()
    shapes, _ = zip(*shapes_imgs)
    print 'type(shapes): ', type(shapes)
    print 'type(shapes[0]): ', type(shapes[0])
    # align all
    shapes = shape_transform.align_all_shapes(list(shapes))
    xy_lim = 0.2
    ax = show_data.get_new_axis()
    for shape in shapes:
        show_data.plot_shape_full(ax, shape, 'r', ylim=[-xy_lim,xy_lim], xlim=[-xy_lim,xy_lim])
    mean_shape = shape_utils.mean_shape(shapes)
    show_data.plot_shape_points(ax, mean_shape, 'ko')
    print 'mean_shape.norm(): ', mean_shape.norm()
    plt.show()




def main():
    """
    MAIN
    """
    # test_visulize_transform()
    # test_simple_calc_transform_params()
    # test_simple_align_all_shapes()
    test_teeth_align_all_shapes()





if __name__ == '__main__':
    main()