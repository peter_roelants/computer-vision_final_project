#####################################################################
# Test sampling of points
#####################################################################

import cv2
import numpy as np
from matplotlib import pyplot as plt
import math

import sample_point
from shape import Shape
import show_data
import read_data
import filters
import utils


def gaussian_img(size, sigma):
    """
    Return an image with some kind of gaussian gradient for testing.
    """
    gaus = cv2.getGaussianKernel(size*2, sigma)
    half_size = size / 3
    gaus = gaus[0+half_size:size+half_size]
    img = np.ndarray(shape=(size, size))
    for i in xrange(size):
        img[:,] = gaus[:]
    return img


def test_sample_line():
    """
    Test the getting of a sample line.
    """
    img = gaussian_img(11, 1)
    img[2,:] = np.array([img[8,0]]*11)
    print('img[:,0]: ' + str(img[:,0]))
    line = sample_point.sample_line(img, (5,5), (0.7,0.7), 5, 1)
    print('line: ' + str(line))
    print('line.shape: ' + str(line.shape))
    print('img.shape: ' + str(img.shape))
    fig = plt.figure()
    ax = fig.add_subplot(1,2,1)
    ax.imshow(img, cmap = 'gray', interpolation = 'nearest')
    ax = fig.add_subplot(1,2,2)
    plt.imshow(np.matrix(line).T, cmap = 'gray', interpolation = 'nearest')
    plt.show()


def test_get_normals():
    """
    Show how the normal is sampled
    """
    square_matrix = np.array(
        [[1, 1],
         [-1, 1],
         [-1, -1],
         [1, -1]])
    shape = Shape(square_matrix)
    print 'shape.xy_2D().shape: ', shape.xy_2D().shape
    points_normals = sample_point.get_normals(shape)
    print 'points_normals: ', points_normals
    for point, normal in points_normals:
        print point
        print normal
        plt.plot(point[0], point[1], 'ro')
        plt.plot([point[0], point[0]+normal[0]*1], [point[1], point[1]+normal[1]*1], color='k', linestyle='-', linewidth=1)
    plt.show()


def test_getNormal():
    """
    Test the getting of a normal
    """
    prev = np.array([1, 0])
    point = np.array([2, 0])
    next = np.array([3, 0])
    normal = sample_point.getNormal(prev, point, next)
    print 'normal: ', normal
    print 'normal.shape: ', normal.shape
    prev = np.array([1, 0])
    point = np.array([1, 1])
    next = np.array([1, 2])
    normal = sample_point.getNormal(prev, point, next)
    print 'normal: ', normal
    print 'normal.shape: ', normal.shape
    prev = np.array([1,1])
    point = np.array([2, 2])
    next = np.array([3, 3])
    normal = sample_point.getNormal(prev, point, next)
    print 'normal: ', normal
    print 'normal.shape: ', normal.shape


def test_sample_line_from_split_shape():
    """
    Test the sampling of a splitted shape
    """
    gaus = cv2.getGaussianKernel(31, 2)
    print('gaus.shape: ' + str(gaus.shape))
    gaus = gaus.dot(gaus.T)
    # Create shape
    c5 = math.cos(0.785398163)* 5
    s5 = math.sin(0.785398163)*5
    square_matrix = np.array(
        [[15,20],
         [15+c5, 15+s5],
         [20,15],
         [15+c5, 15-s5],
         [15,10],
         [15-c5, 15-s5],
         [10,15],
         [15-c5, 15+s5]])
    shape = Shape(square_matrix)
    list_of_points = sample_point._sample_line_from_split_shape(gaus, shape, 10, 1)
    print('len(list_of_points): ' + str(len(list_of_points)))
    print('list_of_points[0][0].shape: ' + str(list_of_points[0][0].shape))
    result = np.ndarray(shape=(len(list_of_points), list_of_points[0][0].shape[0]))
    for p_idx in xrange(len(list_of_points)):
        result[p_idx] = list_of_points[p_idx][0][:]
    print('result.shape: ' + str(result.shape))
    fig = plt.figure()
    ax = fig.add_subplot(1,2,1)
    ax.imshow(gaus, cmap = 'gray', interpolation = 'nearest')
    show_data.plot_shape_points(ax, shape, 'r+')
    ax = fig.add_subplot(1,2,2)
    ax.imshow(result, cmap = 'gray', interpolation = 'nearest')
    plt.show()


def test_sample_line_full_shape():
    """
    Test the sampling of a full shape
    """
    gaus = cv2.getGaussianKernel(31, 2)
    print('gaus.shape: ' + str(gaus.shape))
    gaus = gaus.dot(gaus.T)
    # Create 2 squares
    square_matrix_1 = np.array(
        [[15,20],
         [20,15],
         [15,10],
         [10,15]])
    shape_1 = Shape(square_matrix_1)
    square_matrix_2 = np.array(
        [[15,15],
         [15,20],
         [20,20],
         [20,15]])
    shape_2 = Shape(square_matrix_2)
    shape_1.merge(shape_2)
    shape = shape_1
    list_of_points = sample_point.sample_line_full_shape(gaus, shape, 10, 1)
    print('len(list_of_points): ' + str(len(list_of_points)))
    print 'list_of_points: ', list_of_points
    print('len(list_of_points[0]): ' + str(len(list_of_points[0])))
    print('list_of_points[0][0].shape: ' + str(list_of_points[0][0].shape))
    result = np.ndarray(shape=(len(list_of_points), list_of_points[0][0].shape[0]))
    for p_idx in xrange(len(list_of_points)):
        result[p_idx] = list_of_points[p_idx][0][:]
    print('result.shape: ' + str(result.shape))
    fig = plt.figure()
    ax = fig.add_subplot(1,2,1)
    ax.imshow(gaus, cmap = 'gray', interpolation = 'nearest')
    show_data.plot_shape_points(ax, shape, 'r+')
    ax = fig.add_subplot(1,2,2)
    ax.imshow(result.T, cmap = 'gray', interpolation = 'nearest')
    plt.show()


def show_sample_lines_teeth():
    """
    Show the lines for a group of teeth
    """
    k = 10
    stepsize = 1
    file_nb = 1
    mirror = False
    # sample lines
    (shape, img) = read_data.get_shape_img(file_nb, mirror)
    list_of_points = sample_point.sample_line_full_shape(img, shape, k, stepsize)
    result = np.ndarray(shape=(len(list_of_points), list_of_points[0][0].shape[0]))
    for p_idx in xrange(len(list_of_points)):
        result[p_idx] = list_of_points[p_idx][0][:]
    print('result.shape: ' + str(result.shape))
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.imshow(result, cmap = 'gray', interpolation = 'nearest')
    plt.show()


def show_sample_lines_tooth():
    """
    Show the lines for a group of teeth
    """
    k = 10
    stepsize = 1
    file_nb = 1
    segment_nb = 1
    mirror = False
    # sample lines
    (shape, img) = read_data.get_segment_img(file_nb, segment_nb, mirror)
    list_of_points = sample_point.sample_line_full_shape(img, shape, k, stepsize)
    result = np.ndarray(shape=(len(list_of_points), list_of_points[0][0].shape[0]))
    for p_idx in xrange(len(list_of_points)):
        result[p_idx] = list_of_points[p_idx][0][:]
    print('result.shape: ' + str(result.shape))
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.imshow(result, cmap = 'gray', interpolation = 'nearest')
    plt.show()


def show_build_shape_point_lines_model(filter_fun=None):
    """
    Test the building of the point models
    """
    shape_img_list = read_data.get_all_8_teeth_imgs()
    if filter_fun != None:
        shape_img_list = filters.filter_shape_img_list(shape_img_list, filter_fun)
    print('len(shape_img_list): ' + str(len(shape_img_list)))
    img_shape = shape_img_list[0]
    print('img_shape[0].nb_of_points(): ' + str(img_shape[0].nb_of_points()))
    print('img_shape[1].shape: ' + str(img_shape[1].shape))
    point_models = sample_point.build_shape_point_lines_model(shape_img_list, 25, 1.5, utils.normalize_min_max)
    print('point_models[0][0].shape: ' + str(point_models[0][0].shape))
    print('point_models[0][1].shape: ' + str(point_models[0][1].shape))
    mean_result = np.ndarray(shape=(len(point_models), point_models[0][0].shape[0]))
    print('mean_result.shape: ' + str(mean_result.shape))
    for p_idx in xrange(len(point_models)):
        mu = point_models[p_idx][0]
        mean_result[p_idx,:] = mu[:]
    plt.imshow(mean_result.T, cmap = 'gray', interpolation = 'nearest')
    plt.ylabel('Sample pixel point')
    plt.xlabel('Landmark point')
    plt.show()


def test_compare_line():
    """
    Test the comparision of lines in a visual way.
    """
    fig = plt.figure()
    ax = fig.add_subplot(2,2,1)
    # Get an image with a gradient
    img = gaussian_img(11, 1)
    ax.imshow(img, cmap = 'gray', interpolation = 'nearest')
    k = 2
    m = 5
    # get a line that will be used as mean
    mean_line = sample_point.sample_line(img, (5,2), (0,1), k, 1)
    ax = fig.add_subplot(2,2,2)
    ax.imshow(np.matrix(mean_line), cmap = 'gray', interpolation = 'nearest')
    # Sample a line to compare with
    new_sample_line = sample_point.sample_line(img, (5,5), (0,1), m, 1)
    ax = fig.add_subplot(2,2,3)
    ax.imshow(np.matrix(new_sample_line), cmap = 'gray', interpolation = 'nearest')
    # Covariance matrix is identity matrix
    cov = np.identity(k+2+1)
    print('mean_line.shape: ' + str(mean_line.shape))
    print 'mean_line: ', mean_line
    print('new_sample_line.shape: ' + str(new_sample_line.shape))
    print 'new_sample_line', new_sample_line
    print('cov.shape: ' + str(cov.shape)) 
    # Compare the lines
    nb_of_shifts, Md = sample_point.compare_line(mean_line, cov, new_sample_line, 1)
    print("nb_of_shifts: " + str(nb_of_shifts))
    print("Md: " + str(Md))
    # Extract a mach and plot them next to eachother
    match = new_sample_line[m+nb_of_shifts-k : m+nb_of_shifts+k+1]
    print('match.shape: ' + str(match.shape))
    print 'match: ', match
    compare_img = np.matrix([mean_line, match])
    ax = fig.add_subplot(2,2,4)
    ax.imshow(compare_img, cmap = 'gray', interpolation = 'nearest')
    plt.show()


def test_compare_figure_lines(filter_fun=None):
    """
    Test the comparison of a figure to the model
    """
    k = 10
    m = 30
    step_size = 1
    # Get model
    shape_img_list = read_data.get_all_8_teeth_imgs()
    if filter_fun != None:
        shape_img_list = filters.filter_shape_img_list(shape_img_list, filter_fun)
    point_models = sample_point.build_shape_point_lines_model(shape_img_list, k, step_size)
    # Get sample lines from a shape
    # get image and shape
    (shape, img) = shape_img_list[0]
    # sample lines
    lines = sample_point.sample_line_full_shape(img, shape, m, step_size)
    print('len(lines): ' + str(len(lines)))
    print('lines[0][0].shape: ' + str(lines[0][0].shape))
    # compare the figure with the model
    point_movements = sample_point.compare_figure_lines(point_models, lines, 1)
    print 'point_movements[0]: ', point_movements[0]
    new_shape = sample_point.get_new_points(point_movements, step_size, shape)

    # show the image and plot movements
    ax = show_data.get_new_axis()
    show_data.plot_img(ax, img)
    show_data.plot_shape_points(ax, shape, 'bo')
    show_data.plot_shape_points(ax, new_shape, 'ro')
    plt.ylim([500,1400])
    plt.xlim([1200,1800])
    plt.show()




def main():
    """
    MAIN
    """
    # test_sample_line()
    # test_getNormal()
    # test_get_normals()
    # test_sample_line_from_split_shape()
    # test_sample_line_full_shape()
    # show_sample_lines_teeth()
    # show_sample_lines_tooth()
    # test_compare_line()
    # test_compare_figure_lines()

    # show_build_shape_point_lines_model(filters.filter_function)
    show_build_shape_point_lines_model()




if __name__ == '__main__':
    main()