# Landmark file info:
# ./Data/Landmarks/Original
# This directory contains a .txt file for each ground truth. As there are 14 images, each containing 8 incisors, 112 files are present in total. Each teeth is resampled to 40 landmarks. Landmarks are given in image coordinates, and are ordered as follows: x1, y1, x2, y2, ... , x40, y40.
# ./Data/Landmarks/Mirrored
# This directory contains landmarks for the same radiographs as the original landmarks, only this time the images were mirrored along a vertical axis.

# Radiograph file info
#./Data/Radiographs
# This directory contains 14 .tif images, each one representing a dental radiograph. These images have ground truths, and corresponding landmarks.
# This directory also contains an 'extra' folder, with 16 more dental radiographs. For these images, no ground truths or landmarks are available. You are free to use these images as you wish.

#####################################################################
# This file contains methods to read landmark and image data
#####################################################################

import numpy as np
import cv2
from shape import Shape



def get_landmarks_for_segment(file_nb, segment_nb, is_mirrored, img=None):
    """
    Return the landmarks for the given file and segment as a numpy array.
    Input:
        file_nb: number of the file (this should correspond with the number 
            of the file from the Radiographs directory)
        segment_nb: the number of the segment (this should correspond witht 
            the number of the segment from the Segmentations directory)
        is_mirrored: is the segment mirrored
        img: if the image is passed into this function, the mirrored shapes
            are correctly translated.
    Output:
        Shape
    """
    landmark_dir = '../../_Data/Landmarks/'
    # Set the directory depending if the segment is mirrored or not.
    if is_mirrored:
        landmark_dir = landmark_dir + 'mirrored/'
        file_nb += 14
    else:
        landmark_dir = landmark_dir + 'original/'
    # make the filename and open the file
    file_name = 'landmarks' + str(file_nb) + '-' + str(segment_nb) + '.txt'
    segment_file = open(landmark_dir + file_name)
    # get all the landmarks
    lms = np.loadtxt(segment_file, dtype='float')
    # translate image if is mirrored and img is not None
    if is_mirrored and img != None:
        lms[::2] = lms[::2] + img.shape[1]
    segment_file.close()
    return Shape(lms)


def get_8_teeth(file_nb, is_mirrored, img=None):
    """
    Return the landmarks of the 8 teeth for the given file as a numpy array.
    Input:
        file_nb: number of the file (this should correspond with the number 
            of the file from the Radiographs directory)
        is_mirrored: is the segment mirrored
        img: if the image is passed into this function, the mirrored shapes
            are correctly translated.
    Output:
        Shape
    """
    shape = Shape()
    # get the landmarks of the segments of te shape
    for i in xrange(1,9):
        shape.merge(get_landmarks_for_segment(file_nb, i, is_mirrored, img))
    return shape

def get_lower_teeth(file_nb, is_mirrored, img=None):
    """
    Return the landmarks of the 4 lower teeth for the given file as a numpy array.
    Input:
        file_nb: number of the file (this should correspond with the number 
            of the file from the Radiographs directory)
        is_mirrored: is the segment mirrored
        img: if the image is passed into this function, the mirrored shapes
            are correctly translated.
    Output:
        Shape
    """
    shape = Shape()
    # get the landmarks of the segments of te shape
    for i in xrange(1,5):
        shape.merge(get_landmarks_for_segment(file_nb, i, is_mirrored, img))
    return shape

def get_upper_teeth(file_nb, is_mirrored, img=None):
    """
    Return the landmarks of the 4 upper teeth for the given file as a numpy array.
    Input:
        file_nb: number of the file (this should correspond with the number 
            of the file from the Radiographs directory)
        is_mirrored: is the segment mirrored
        img: if the image is passed into this function, the mirrored shapes
            are correctly translated.
    Output:
        Shape
    """
    shape = Shape()
    # get the landmarks of the segments of te shape
    for i in xrange(5,9):
        shape.merge(get_landmarks_for_segment(file_nb, i, is_mirrored, img))
    return shape


def get_img(file_nb, is_mirrored):
    """
    Get the image of the given file.
    """
    img_dir = '../../_Data/Radiographs/'
    img_name = '{:0>2d}.tif'.format(file_nb)
    file_name = img_dir + img_name
    img = cv2.imread(file_name, 0)
    if is_mirrored:
        img = np.fliplr(img)
    return img


def get_shape_img(file_nb, mirror):
    """
    Return the shape and image for the given file_nb.
    """
    img = get_img(file_nb, mirror)
    shape = get_8_teeth(file_nb, mirror, img)
    return (shape, img)

def get_lower_teeth_img(file_nb, mirror):
    """
    Return the shape and image for the given file_nb.
    """
    img = get_img(file_nb, mirror)
    shape = get_lower_teeth(file_nb, mirror, img)
    return (shape, img)

def get_upper_teeth_img(file_nb, mirror):
    """
    Return the shape and image for the given file_nb.
    """
    img = get_img(file_nb, mirror)
    shape = get_upper_teeth(file_nb, mirror, img)
    return (shape, img)


def get_all_8_teeth_imgs():
    """
    Get all 8 teeth and corresponding images
    Output:
        shapes_imgs: list of shapes and images
            [(shape, img), ...]
    """
    shapes_imgs = [None] * 14 * 2
    idx = 0
    for mirror in [False, True]:
        for file_nb in xrange(1,15):
            print('get_all_8_teeth_imgs: ' + str(file_nb) + ', ' + str(mirror))
            shapes_imgs[idx] = get_shape_img(file_nb, mirror)
            idx += 1
    return shapes_imgs


def get_all_8_teeth_imgs_except(file_nbs_leave_out):
    """
    Get all 8 teeth and corresponding images except of the given file_nbs
    Input: 
        file_nb_leave_out: nb of the file to leave out
    Output:
        shapes_imgs: list of shapes and images
            [(shape, img), ...]
    """
    shapes_imgs = [None] * 2 * (14 - len(file_nbs_leave_out))
    idx = 0
    for mirror in [False, True]:
        for file_nb in xrange(1,15):
            if not file_nb in file_nbs_leave_out:
                print('get_all_8_teeth_imgs: ' + str(file_nb) + ', ' + str(mirror))
                shapes_imgs[idx] = get_shape_img(file_nb, mirror)
                idx += 1
    return shapes_imgs


def get_all_upper_teeth_imgs():
    """
    Get all 8 teeth and corresponding images
    Output:
        shapes_imgs: list of shapes and images
            [(shape, img), ...]
    """
    shapes_imgs = [None] * 14 * 2
    idx = 0
    for mirror in [False, True]:
        for file_nb in xrange(1,15):
            print('get_all_upper_teeth_imgs: ' + str(file_nb) + ', ' + str(mirror))
            shapes_imgs[idx] = get_upper_teeth_img(file_nb, mirror)
            idx += 1
    return shapes_imgs


def get_all_upper_teeth_imgs_except(file_nbs_leave_out):
    """
    Get all 8 teeth and corresponding images except of the given file_nbs
    Output:
        shapes_imgs: list of shapes and images
            [(shape, img), ...]
    """
    shapes_imgs = [None] * 2 * (14 - len(file_nbs_leave_out))
    idx = 0
    for mirror in [False, True]:
        for file_nb in xrange(1,15):
            if not file_nb in file_nbs_leave_out:
                print('get_all_upper_teeth_imgs_except: ' + str(file_nb) + ', ' + str(mirror))
                shapes_imgs[idx] = get_upper_teeth_img(file_nb, mirror)
                idx += 1
    return shapes_imgs


def get_all_lower_teeth_imgs():
    """
    Get all 8 teeth and corresponding images
    Output:
        shapes_imgs: list of shapes and images
            [(shape, img), ...]
    """
    shapes_imgs = [None] * 14 * 2
    idx = 0
    for mirror in [False, True]:
        for file_nb in xrange(1,15):
            print('get_all_lower_teeth_imgs: ' + str(file_nb) + ', ' + str(mirror))
            shapes_imgs[idx] = get_lower_teeth_img(file_nb, mirror)
            idx += 1
    return shapes_imgs

def get_all_lower_teeth_imgs_except(file_nbs_leave_out):
    """
    Get all 8 teeth and corresponding images except of the given file_nbs
    Input: 
        file_nb_leave_out: nb of the file to leave out
    Output:
        shapes_imgs: list of shapes and images
            [(shape, img), ...]
    """
    shapes_imgs = [None] * 2 * (14 - len(file_nbs_leave_out))
    idx = 0
    for mirror in [False, True]:
        for file_nb in xrange(1,15):
            if not file_nb in file_nbs_leave_out:
                print('get_all_lower_teeth_imgs_except: ' + str(file_nb) + ', ' + str(mirror))
                shapes_imgs[idx] = get_lower_teeth_img(file_nb, mirror)
                idx += 1
    return shapes_imgs



def get_segment_img(file_nb, segment_nb, mirror):
    """
    Return the shape and image for the given file_nb.
    """
    img = get_img(file_nb, mirror)
    segment = get_landmarks_for_segment(file_nb, segment_nb, mirror, img)
    return (segment, img)


def get_all_segment_imgs(segment_nb):
    """
    Get all segment and corresponding images 
    Output:
        shapes_imgs: list of shapes and images
            [(segment, img), ...]
    """
    shapes_imgs = [None] * 14 * 2
    idx = 0
    for mirror in [False, True]:
        for file_nb in xrange(1,15):
            print('get_all_segment_imgs: ' + str(file_nb) + ', ' + str(segment_nb) + ', '  + str(mirror))
            shapes_imgs[idx] = get_segment_img(file_nb, segment_nb, mirror)
            idx += 1
    return shapes_imgs


def get_all_segment_imgs_except(segment_nb, file_nbs_leave_out):
    """
    Get all segment and corresponding images except of the given file_nbs
    Output:
        shapes_imgs: list of shapes and images
            [(segment, img), ...]
    """
    shapes_imgs = [None] * 2 * (14 - len(file_nbs_leave_out))
    idx = 0
    for mirror in [False, True]:
        for file_nb in xrange(1,15):
            if not file_nb in file_nbs_leave_out:
                print('get_all_segment_imgs_except: ' + str(file_nb) + ', ' + str(segment_nb) + ', '  + str(mirror))
                shapes_imgs[idx] = get_segment_img(file_nb, segment_nb, mirror)
                idx += 1
    return shapes_imgs



def main():
    """
    MAIN
    """
    for i in xrange(1,15):
        img = get_img(i, False)
        print'img.shape: ', img.shape



if __name__ == '__main__':
    main()