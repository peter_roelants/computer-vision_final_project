#####################################################################
# Active shape model tests
#####################################################################

from matplotlib import pyplot as plt

import active_shape_model
import show_data
import shape_transform
import read_data
import pca_model



def test_update_shape_model():
    """
    Test the updating of a shape model
    """
    nb_of_pc = 10
    shapes_imgs = read_data.get_all_8_teeth_imgs()
    original_shapes, _ = zip(*shapes_imgs)
    print 'type(shapes): ', type(original_shapes)
    print 'type(shapes[0]): ', type(original_shapes[0])
    # align all
    shapes = shape_transform.align_all_shapes(list(original_shapes))
    print 'len(shapes): ', len(shapes)
    # do pca
    eigenvalues, eigenvectors, mean_shape = pca_model.pca_of_shapes(shapes, nb_of_pc)
    #
    target_shape = original_shapes[0]
    #
    b = pca_model.get_empty_model(nb_of_pc)
    # update shape model
    cov  = pca_model.get_cov_mean_all_models(shapes, eigenvectors)
    print 'cov.shape: ', cov.shape
    mean_b = pca_model.get_empty_model(nb_of_pc)
    print 'mean_b.shape: ' , mean_b.shape
    b, (tx, ty), s, theta = active_shape_model.update_shape_model_bounded(b, mean_shape, eigenvectors, eigenvalues, target_shape, cov, mean_b)
    print 'b, (tx, ty), s, theta: ', b, (tx, ty), s, theta
    new_shape = pca_model.reconstruct_shape(b, eigenvectors, mean_shape)
    new_shape = new_shape.transform(tx, ty, s, theta)
    # plot shapes
    ax = show_data.get_new_axis() 
    show_data.plot_shape_full(ax, target_shape, 'r')
    show_data.plot_shape_full(ax, new_shape, 'g')
    plt.show()





def main():
    """
    MAIN
    """
    test_update_shape_model()





if __name__ == '__main__':
    main()